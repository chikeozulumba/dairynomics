[![pipeline status](https://gitlab.com/UjoNyawara/dairynomics/badges/master/pipeline.svg)](https://gitlab.com/UjoNyawara/dairynomics/commits/master)
[![coverage report](https://gitlab.com/UjoNyawara/dairynomics/badges/master/coverage.svg)](https://gitlab.com/UjoNyawara/dairynomics/commits/master)
[![codebeat badge](https://codebeat.co/badges/704845cf-77c3-4f20-a9f2-33712232eeaf)](https://codebeat.co/a/njunge-njenga/projects/gitlab-com-ujonyawara-dairynomics-master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/4f383e6e-b102-4808-afb1-a3bc9dab2839/deploy-status)](https://app.netlify.com/sites/dairynomics/deploys)
# dairynomics-ui

> The Dairynomics UI

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
