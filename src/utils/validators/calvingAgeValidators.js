export default (value) => {
  if (value === '') {
    return {
      isError: true,
      message: 'Age of first calving is required'
    }
  }

  if (value < 24 || value > 40) {
    return {
      isError: true,
      message: `Age must be between 24-40 months `
    }
  }

  return {
    isError: false,
    message: ''
  }
}
