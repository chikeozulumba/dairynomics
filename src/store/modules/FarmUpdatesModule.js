import AxiosCalls from '@/utils/api/AxiosCalls'

export default {
  state: {
    farmUpdateList: [],
    farmId: null,
    farmUpdatesError: null,
    farmDetail: null,
    currentPage: null,
    lastPage: null
  },
  getters: {
    GET_FARM_UPDATES (state) {
      return [...state.farmUpdateList]
    },
    GET_FARM_DETAIL (state) {
      return {...state.farmDetail}
    },
    GET_CURRENT_PAGE (state) {
      return state.currentPage
    },
    GET_LAST_PAGE (state) {
      return state.lastPage
    }
  },
  mutations: {
    POPULATE_FARM_UPDATES (state, payload) {
      state.farmUpdateList = [...payload]
    },
    POPULATE_FARM_UPDATES_ERROR (state, payload) {
      state.farmUpdatesError = payload
    },
    CHANGE_FARM_ID (state, payload) {
      state.farmId = payload
    },
    POPULATE_FARM_DETAIL (state, payload) {
      state.farmDetail = payload
    },
    POPULATE_CURRENT_PAGE (state, payload) {
      state.currentPage = payload
    },
    POPULATE_LAST_PAGE (state, payload) {
      state.lastPage = payload
    }
  },
  actions: {
    async fetchFarmUpdates ({commit, state}, pageNumber) {
      let response
      if (pageNumber) {
        response = await AxiosCalls.get(`api/v1/farms/${state.farmId}/updates?page=${pageNumber}`)
      } else {
        response = await AxiosCalls.get(`api/v1/farms/${state.farmId}/updates`)
      }
      if (response.data.success) {
        commit('POPULATE_FARM_UPDATES', response.data.data.updates)
        commit('POPULATE_CURRENT_PAGE', response.data.meta.current_page)
        commit('POPULATE_LAST_PAGE', response.data.meta.last_page)
        commit('POPULATE_FARM_DETAIL', response.data.farm)
      } else {
        commit('POPULATE_FARM_UPDATES_ERROR', response)
      }
    },
    SET_FARM_ID ({commit}, id) {
      commit('CHANGE_FARM_ID', id)
    }
  }
}
