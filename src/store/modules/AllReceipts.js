import router from '@/router/router'
import axios from 'axios'
import { ecommerceBaseUrl } from '@/utils/api/urls'

const url = `${ecommerceBaseUrl}/api/receipts`

export default {
  state: {
    receipts: [],
    isLoading: false
  },
  mutations: {
    SET_RECEIPTS (state, receipts) {
      state.receipts = receipts
    },
    SET_LOADER (state, payload) {
      state.isLoading = payload
    }
  },
  actions: {
    getAllReceipts ({ commit }) {
      commit('SET_LOADER', true)
      const token = localStorage.getItem('token')
      return axios
        .get(url, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(response => {
          commit('SET_RECEIPTS', response.data.receipts)
          commit('SET_LOADER', false)
        })
        .catch(error => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    }
  }
}
