import AxiosCalls from '@/utils/api/AxiosCalls'
import {
  alertError,
  getAPIErrorMessage
} from '@/utils/helperFunctions'

export default {
  state: {
    livestockDetials: {}
  },
  getters: {
    avalaibleliveStock (state) {
      return {...state.livestockDetials}
    }
  },
  mutations: {
    LIVE_STOCK_DETAILS (state, data) {
      const payload = {
        weigth: data.weight,
        id: data.id,
        price: data.price,
        postOn: data.created_at.date,
        bodyCondition: data.body_condition,
        description: data.description,
        sex: data.sex,
        fieldTagNum: data.tag_num,
        age: data.age,
        breed: data.breed,
        images: data.photo,
        county: data.county
      }
      state.livestockDetials = {...payload}
    }
  },
  actions: {
    getIndividualLivestock: async (context, id) => {
      let data = await AxiosCalls.awaitGet(`livestocks/${id}`)
      if (data.error === 404) {
        window.location.href = '/404'
      }
      if (data.error) return alertError(getAPIErrorMessage(data.error))
      context.commit('LIVE_STOCK_DETAILS', data)
    }
  }
}
