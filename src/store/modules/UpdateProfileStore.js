import axios from 'axios'
import router from '@/router/router'

export default {
  state: {
    userUpdateStatus: false,
    updateError: null,
    updateSuccess: false,
    getUserDetails: {},
    successMessage: null
  },
  mutations: {
    setUserUpdateStatus (state) {
      state.userUpdateStatus = true
      return state.userUpdateStatus
    },
    UpdateErrors: (state, errorMessage) => {
      state.userUpdateStatus = 0
      state.updateError = errorMessage
    },
    ProfileSuccess (state, message) {
      state.updateSuccess = true
      state.successMessage = message
      return state
    },
    Details: (state, user) => (
      state.getUserDetails = user)
  },
  actions: {
    editUser ({commit}, updateData) {
      commit('setUserUpdateStatus')
      commit('ProfileSuccess', '')
      const userId = localStorage.getItem('userId')
      return axios({
        url: `https://beta.cowsoko.com/api/v1/user/${userId}`,
        data: updateData,
        method: 'post'
      })
        .then((response) => {
          if (response.data.success === true) {
            commit('ProfileSuccess', 'Profile updated successfully')
            router.push('/user-profile')
            commit('UpdateErrors')
          } else {
            commit('UpdateErrors', 'Update Profile unsuccessful , Please try again')
            commit('ProfileSuccess', '')
          }
        })
        .catch(error => {
          if (error.response.data.message === 'Old Password Details missing') {
            commit('UpdateErrors', 'Current password details missing')
          } else if (error.response.data.message === 'Wrong Old Password Details') {
            commit('UpdateErrors', 'Wrong current password details')
          } else {
            commit('UpdateErrors', 'Please provide current and new pin to update your  pin')
          }
        })
    },
    async userDetails ({commit}) {
      try {
        const token = localStorage.getItem('token')
        const userId = localStorage.getItem('userId')
        const response = await axios.get(`https://beta.cowsoko.com/api/v1/user/${userId}`,
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        commit('Details', response.data.data)
      } catch (error) {
        commit('UpdateErrors', 'Unable to get your user information, Please try again ')
      }
    }
  },

  getters: {
    getUserUpdateStatus (state) {
      return {...state}
    }
  }
}
