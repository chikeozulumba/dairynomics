import axios from 'axios'
export default {
  state: {
    invoices: [],
    loading: false,
    singleInvoice: {},
    orderList: [],
    billing: {}
  },
  mutations: {
    SETSTATTETOINVOICES: (state, invoices) => {
      state.invoices = invoices
    },
    SHOWLOADING: (state, value) => {
      state.loading = value
    },
    SETSINGLEINVOICE: (state, singleInvoce) => {
      state.singleInvoice = singleInvoce
    },
    BIILLINGINFOR: (state, billing) => {
      state.billing = billing
    },
    RESETINVOICE: state => {
      state.singleInvoce = {}
    },
    SETORDERLIST: (state, orderlist) => {
      state.orderList = orderlist
    }
  },
  getters: {
    GETINVOICES: state => {
      return state.invoices.filter(invoices => {
        if (invoices.paymentStatus === 'pending') {
          return invoices
        }
      })
    }
  },
  actions: {
    getAllInvoices: async ({ commit }) => {
      commit('SHOWLOADING', true)
      try {
        const token = localStorage.getItem('token')
        const invoices = await axios.get(
          `https://dairynomics.herokuapp.com/api/invoices/`,
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        commit('SETSTATTETOINVOICES', invoices.data.invoices.rows)
        commit('SHOWLOADING', false)
      } catch (errors) {
        return false
      }
    },
    getSingleInvoice: async ({ commit }, { invoiceId }) => {
      const token = localStorage.getItem('token')
      commit('SHOWLOADING', true)
      const singleInvoice = await axios.get(
        `https://dairynomics.herokuapp.com/api/invoices/${invoiceId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      commit('SETSINGLEINVOICE', singleInvoice.data.invoice)
      commit('BIILLINGINFOR', singleInvoice.data.invoice.order.billing)
      commit('SETORDERLIST', singleInvoice.data.invoice.order.orderItems)
      commit('SHOWLOADING', false)
    },
    resetInvoice: ({ commit }) => {
      commit('RESETINVOICE')
    }
  }
}
