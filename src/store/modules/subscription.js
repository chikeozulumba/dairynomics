import AxiosCalls from '@/utils/api/AxiosCalls'

export default {
  state: {
    subscriptionDetails: {},
    isLoading: false,
    hasPlan: null
  },
  mutations: {
    subscriptionDetails: (state, subscriptionData) => {
      state.subscriptionDetails = subscriptionData
      return state.subscriptionDetails
    },
    loading: (state, payload) => {
      state.isLoading = payload
      return state.isLoading
    }
  },
  actions: {
    getSubscriptionDetails ({ commit }, { router }) {
      commit('loading', true)
      return AxiosCalls.getEcommerce('subscriptions/user')
        .then(response => {
          commit('subscriptionDetails', response)
          commit('loading', false)
        })
        .catch(() => {
          return router.push('/')
        })
    },

    subscribeServiceProvider () {
      return AxiosCalls.postEcommerce('subscriptions')
        .then({})
        .catch({})
    }
  },
  getters: {
    subscriptionInfo (state) {
      return { ...state }
    }
  }
}
