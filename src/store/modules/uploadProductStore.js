import AxiosCalls from '@/utils/api/AxiosCalls'
import {getUserId} from '@/utils/helperFunctions'

export default {
  state: {
    loading: false,
    error: '',
    message: '',
    userData: {},
    currentComponent: 'ProductSection',
    products: []
  },
  getters: {
    submitFormDetails (state) {
      return state
    }
  },
  mutations: {
    SUBMIT_FORM_LOADING (state) {
      state.loading = true
    },
    SUBMIT_FORM_SUCCESS (state, payload) {
      state.userData = payload
      state.loading = false
    },
    SUBMIT_FORM_ERROR (state, payload) {
      state.error = payload
      state.loading = false
    },
    CHANGE_COMPONENT (state, payload) {
      state.currentComponent = payload
    },
    FETCH_PRODUCTS_LOADING (state) {
      state.loading = true
    },
    FETCH_PRODUCTS_SUCCESS (state, payload) {
      state.products = payload.products
      state.message = payload.message
      state.loading = false
    },
    FETCH_PRODUCTS_ERROR (state, payload) {
      state.error = payload
      state.loading = false
    }
  },
  actions: {
    submitForm: async ({ dispatch, commit }, payload) => {
      commit('SUBMIT_FORM_LOADING')
      try {
        let { data } = await AxiosCalls.axiosPost('products', payload)
        commit('SUBMIT_FORM_SUCCESS', data)
        await dispatch('fetchMyProducts')
        commit('CHANGE_COMPONENT', 'ProductSection')
      } catch (error) {
        commit('SUBMIT_FORM_ERROR', error)
      }
    },
    fetchMyProducts: async ({ commit }) => {
      commit('FETCH_PRODUCTS_LOADING')
      const userId = getUserId()
      const {data, message} = await AxiosCalls.awaitGet(`users/${userId}/products`)
      if (data) {
        const { products, message } = data
        commit('FETCH_PRODUCTS_SUCCESS', { products, message })
      } else {
        commit('FETCH_PRODUCTS_ERROR', message)
      }
    }
  }
}
