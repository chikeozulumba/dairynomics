import AxiosCalls from '@/utils/api/AxiosCalls'
import getRandomItem from '@/utils/performance/getRandomItem'
import router from '@/router/router'

export default {
  state: {
    publications: [],
    randomPublication: {},
    download: '',
    loading: false,
    error: ''
  },
  getters: {
    USER_PUBLICATION: state => state.publications,
    RANDOM_USER_PUBLICATION: state => state.randomPublication,
    DOWNLOAD_PUBLICATION: state => state.download,
    DOWNLOAD_LOADER: state => state.loading,
    DOWNLOAD_ERROR: state => state.error

  },
  mutations: {
    SET_USER_PUBLICATION (state, payload) {
      state.publications = payload
    },
    SET_RANDOM_USER_PUBLICATION (state, payload) {
      state.randomPublication = payload
    },
    PUBLICATION_ERROR (state, error) {
      state.error = error
    },
    DOWNLOAD_PUBLICATION (state, download) {
      state.download = download
    },
    IS_LOADING (state, loading) {
      state.loading = loading
    }
  },
  actions: {
    GET_USER_PUBLICATION: ({commit}) => {
      return AxiosCalls.get('api/v1/publications')
        .then((response) => {
          const randomPublication = getRandomItem(response.data.data)
          commit('SET_USER_PUBLICATION', response.data.data)
          commit('SET_RANDOM_USER_PUBLICATION', randomPublication)
        })
        .catch((error) => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    },
    GET_DOWNLOAD_PUBLICATION: ({commit}, publication) => {
      commit('IS_LOADING', true)
      const config = {
        responseType: 'blob'
      }
      return AxiosCalls.get(`api/v1/publications/${publication.id}/download`, config)
        .then((response) => {
          if (response.status === 200) {
            const url = window.URL.createObjectURL(new Blob([response.data]))
            const link = document.createElement('a')
            link.href = url
            link.setAttribute('download', `${publication.title}.pdf`)
            document.body.appendChild(link)
            link.click()
            commit('DOWNLOAD_PUBLICATION', response.data)
            commit('IS_LOADING', false)
          } else {
            commit('IS_LOADING', false)
            commit('PUBLICATION_ERROR', response.data)
          }
        })
    }
  }
}
