import axios from 'axios'

export default {
  state: {
    blogs: [],
    currentpage: 0,
    paginatedblogs: [],
    isLoading: false
  },
  mutations: {
    SET_BLOGS (state, blogs) {
      state.blogs = blogs
    },
    SET_PAGINATED_BLOGS (state, blogs) {
      state.paginatedblogs.push(blogs)
    },
    SET_CURRENT_PAGE (state, currentpage) {
      state.currentpage = currentpage
    },
    CLEAR_PAGINATED_BLOGS (state) {
      state.paginatedblogs = []
    },
    SET_LOADER (state, payload) {
      state.isLoading = payload
    }
  },
  getters: {
    getBlogs (state) {
      return {...state}
    }
  },
  actions: {
    getAllBlogs ({commit, state}, pagenumber) {
      const getPayload = (pagenumber, setdata) => {
        commit('SET_LOADER', true)
        axios({
          url: 'https://beta.cowsoko.com/api/v1/blogs?page=' + pagenumber,
          method: 'get',
          headers: {
            authorization: `Bearer ${localStorage.getItem('token')}`
          }
        }).then(results => {
          commit('SET_CURRENT_PAGE', pagenumber)
          commit('SET_BLOGS', results.data)
          if (setdata) {
            commit('CLEAR_PAGINATED_BLOGS')
            commit('SET_PAGINATED_BLOGS', results.data)
          } else {
            commit('SET_PAGINATED_BLOGS', results.data)
          }
          commit('SET_LOADER', false)
        })
      }

      if (state.blogs === 0 || pagenumber > state.paginatedblogs.length) {
        getPayload(pagenumber, false)
      } else if (pagenumber === state.paginatedblogs.length && pagenumber < state.currentpage) {
        getPayload(pagenumber, true)
      } else {
        commit('SET_BLOGS', state.paginatedblogs[pagenumber - 1])
      }
    }
  }
}
