import { shallowMount, createLocalVue } from '@vue/test-utils'
import ExpertBlog from '@/pages/ExpertProfilePage/ExpertBlogs.vue'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

const getters = {
  getExpertBlogs: () => ['joshua', 'fredrick'],
  getExpertBlogsError: () => false
}

const actions = {
  expertBlogs: jest.fn()
}

const $route = {
  name: 'someName',
  params: {id: 1}
}

const $router = {
  push: jest.fn()
}

const store = new Vuex.Store({ getters, actions })

const wrapper = shallowMount(ExpertBlog, { store,
  localVue,
  mocks: {
    $route,
    $router
  }
})
wrapper.vm.$store.dispatch = jest.fn()

const methodSpy = (method) => jest.spyOn(wrapper.vm, method)

const setPagesSpy = methodSpy('setPages')
const getPostsSpy = methodSpy('getPosts')

describe('Expert Blogs Test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('changeProfileSection should set this.activeStatusLink to isMyBlogLinkActive', () => {
    const changeSpy = methodSpy('changeProfileSection')
    changeSpy('joshua')
    expect(wrapper.vm.activeStatusLink).toEqual('isMyBlogLinkActive')
  })

  it('setPages should push index into pages array', () => {
    setPagesSpy()
    expect(wrapper.vm.pages).toBeDefined()
  })

  it('paginate should return posts array', () => {
    const paginateSpy = methodSpy('paginate')
    const posts = ['joshua', 'fredrick', 'david']
    const expectedResult = paginateSpy(posts)
    expect(expectedResult).toBeDefined()
  })

  it('getPosts should set this.posts to posts', () => {
    getPostsSpy('joshua')
    expect(wrapper.vm.posts).toEqual('joshua')
  })

  it('displayedPosts computed should return paginated expertblogs', () => {
    const expectedResult = wrapper.vm.displayedPosts
    expect(expectedResult).toBeDefined()
  })

  it('getExpertBlogs watch should call this.setPages', () => {
    setPagesSpy.mockReset()
    wrapper.vm.$options.watch.getExpertBlogs.call(wrapper.vm, 'new Value')
    expect(setPagesSpy).toHaveBeenCalled()
  })

  it('showLoading should call this.getPosts and return loadingStatus if there is no error', () => {
    const expectedResult = wrapper.vm.showLoading
    expect(getPostsSpy).toHaveBeenCalled()
    expect(expectedResult).toBeFalsy()
  })

  it('showloading should call this.getPosts when there is error', () => {
    const newGetters = {...getters, getExpertBlogsError: () => true}
    const newStore = new Vuex.Store({
      getters: newGetters,
      actions
    })

    const newWrapper = shallowMount(ExpertBlog, {localVue,
      store: newStore,
      mocks: {
        $route,
        $router
      }})
    expect(newWrapper.vm.showLoading).toBeFalsy()
  })
})
