import { shallowMount, createLocalVue } from '@vue/test-utils'
import ExpertFarm from '@/pages/ExpertProfilePage/ExpertFarms.vue'
import UserProfileSwitcher from '@/pages/ExpertProfilePage/Switcher.vue'
import Vuex from 'vuex'
import Router from 'vue-router'

const localVue = createLocalVue()
localVue.use(Vuex)
describe('UserProfileTimelinePage', () => {
  const $route = {
    name: 'someName',
    params: {id: 1}
  }
  const $router = {
    push: jest.fn(),
    params: {id: 1}
  }

  let getters
  let store
  let wrapper
  const router = new Router({push: {name: 'someName'}})
  getters = {
    getExpertFarms: () => {
      return { setExpertFarms: {farm: 'mockedfarm'},
        ExpertProfileError: null}
    }}
  store = new Vuex.Store({getters})
  wrapper = shallowMount(ExpertFarm, { store,
    localVue,
    activeStatusLink: 'mocked',
    router,
    mocks: {
      $route,
      $router
    }
  })
  it('should load farm details', () => {
    const profileSideBarLayout = wrapper.find(ExpertFarm)
    expect(profileSideBarLayout.is(ExpertFarm)).toBe(true)
    expect(wrapper.vm.farmDetails.setExpertFarms.farm).toEqual('mockedfarm')
    wrapper.find(UserProfileSwitcher).vm.$emit('changeProfileSection')
    expect(wrapper.html()).toContain('</userprofileswitcher-stub>')
  })
})
