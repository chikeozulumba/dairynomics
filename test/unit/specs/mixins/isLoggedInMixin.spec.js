import isLoggedInMixin from '@/mixins/isLoggedInMixin'

Storage.prototype.getItem = jest.fn(() => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIn0.Gfx6VO9tcxwk6xqx9yYzSfebfeakZp5JYIgP_edcw_A')

describe('IsLoggedInMixin test', () => {
  it('should return isValid', () => {
    const result = isLoggedInMixin.computed.isLoggedIn()
    expect(result).toBeFalsy()
  })
})
