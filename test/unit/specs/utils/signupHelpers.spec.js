import Vue from 'vue'
import {
  signupHelper
} from '@/utils/signupHelpers.js'
import router from '@/router/router'

const localStorageMockFunction = (mockStore) => {
  const localStorageMock = jest.fn(() => JSON.stringify(mockStore))
  Storage.prototype.getItem = localStorageMock
}

jest.mock('@/router/router')
router.push = jest.fn()
const notifyFn = jest.fn()
Vue.notify = notifyFn

describe('signupHelper function', () => {
  const servicesProvider = {
    nationalId: 1234556,
    expertIn: 'joshua',
    certification: 'Joshua',
    schoolAttended: 'Lagos',
    academic_qualification: 'Degree',
    institution: 'dairynomics',
    dateDuration: {
      from: '2 oct 2018',
      to: '2 oct 2018'
    },
    skill: 'farmer',
    courseStudied: 'Agric',
    training_level: 5
  }

  it('should return path: input_seller/register', () => {
    const mockStore = {
      inputs_offered: 'joshua',
      target_animals: 'ram',
      services_offered: 'service provider'
    }
    localStorageMockFunction(mockStore)
    const accountType = 'I am an input seller'
    const result = signupHelper(accountType)
    expect(result.path).toEqual('input_seller/register')
    expect(result.accountData).toBeTruthy()
  })

  it('should call router.push when inputservices is null', () => {
    Storage.prototype.getItem = jest.fn(() => null)
    const accountType = 'I am an input seller'
    signupHelper(accountType)
    expect(router.push).toHaveBeenCalledWith('/input-seller')
  })

  it('should return path: experts/register', () => {
    localStorageMockFunction(servicesProvider)
    const accountType = 'I am a service provider'
    const result = signupHelper(accountType)
    expect(result.path).toEqual('experts/register')
    expect(result.accountData).toBeTruthy()
  })

  it('should call router.push when service provider is null', () => {
    Storage.prototype.getItem = jest.fn(() => null)
    const accountType = 'I am a service provider'
    signupHelper(accountType)
    expect(router.push).toHaveBeenCalledWith('/service-provider')
  })
})
