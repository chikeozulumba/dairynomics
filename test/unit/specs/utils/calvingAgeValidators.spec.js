import calvingAgeValidators from '@/utils/validators/calvingAgeValidators'

describe('Additional calvingAgeValidators test', () => {
  it('should return isError and message when the value is empty string', () => {
    const expectedResult = calvingAgeValidators('')
    expect(expectedResult.isError).toBeTruthy()
    expect(expectedResult.message).toEqual('Age of first calving is required')
  })
})
