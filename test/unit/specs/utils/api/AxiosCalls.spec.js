import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import AxiosCalls from '@/utils/api/AxiosCalls.js'
jest.mock('@/store')

const baseURL = 'https://beta.cowsoko.com/api/v1/'
const baseUrl = 'https://beta.cowsoko.com'

const mock = new MockAdapter(axios)

describe('Axios Api Calls', () => {
  afterEach(() => {
    mock.reset()
  })
  it("calls the right api when there's no error", () => {
    mock.onGet('https://beta.cowsoko.com/api/v1/blogs').reply(200, {
      blogs: [{ id: 1, title: 'Test title', body: 'Test body' }]
    })
    AxiosCalls.get('api/v1/blogs').then(response => {
      expect(response.data.blogs).toEqual([{ id: 1, title: 'Test title', body: 'Test body' }])
    })
  })

  it('throws an error when there is a network error', () => {
    mock.onGet('https://beta.cowsoko.com/api/v1/blogs').reply(400, new Error('Network Error'))

    AxiosCalls.get('api/v1/blogs').catch((response) => {
      expect(response).toEqual(Error('Request failed with status code 400'))
    })
  })

  it('throws an error when there is a network error', () => {
    mock.onPost('https://beta.cowsoko.com/api/v1/blogs').reply(400, new Error('Network Error'))

    AxiosCalls.axiosPost('blogs', {name: 'joshua'}).catch((response) => {
      expect(response).toEqual(Error('Request failed with status code 400'))
    })
  })

  describe('Axios await calls', () => {
    it('should make axios call and return data', async () => {
      const response = [3]
      mock.onAny(`${baseURL}joshua`).reply(200, response)
      const result = await AxiosCalls.awaitGet('joshua')

      expect(result).toEqual(response)
    })

    it('should make axios call and return error', async () => {
      const error = { status: 404, message: 'error' }
      mock.onGet(`${baseURL}joshua`).reply(404, error)
      const result = await AxiosCalls.awaitGet('joshua')

      expect(result).toEqual({ error: 404, message: 'error' })
    })

    it('should make axios call and return status code of 200 ', () => {
      mock.onPost(`${baseURL}joshua`).reply(200, 'joshua')
      AxiosCalls.axiosPost('joshua', 'joshua').then(response => {
        expect(response.data).toEqual('joshua')
      })
    })

    it('should make axios call and return error of status of 422 ', () => {
      const error = { error: 422, message: 'Service Unavailable' }
      mock.onPost(`${baseURL}joshua`).reply(422, error)
      AxiosCalls.axiosPost('joshua', 'joshua').then({})
        .catch(result => {
          expect(result).toEqual(error)
        })
    })

    it('should make axios call and return error of status of 500 for other error status codes ', () => {
      const error = { error: 500, message: 'Connection Error' }
      mock.onPost(`${baseURL}joshua`).reply(509, error)
      AxiosCalls.axiosPost('joshua', 'joshua').then({})
        .catch(result => {
          expect(result).toEqual(error)
        })
    })

    it('should call store.commit when the status code is 401', () => {
      mock.onGet().reply(401, 'Error')
      AxiosCalls.get('joshua').then({}).catch(result => {
        expect(result).toEqual(Error('Request failed with status code 401'))
      })
    })

    it('should make axios call and return status of 200 ', () => {
      mock.onPut(`${baseUrl}/joshua`).reply(200, 'joshua')
      AxiosCalls.put('joshua', 'joshua').then(response => {
        expect(response.data).toEqual('joshua')
      })
    })

    it('should make axios call and return status of 400 ', () => {
      mock.onPut(`${baseUrl}/joshua`).reply(400, 'joshua')
      AxiosCalls.put('joshua', 'joshua').then({}).catch(error => {
        expect(error).toEqual('joshua')
      })
    })

    it('should make axios call and return status of network error ', () => {
      mock.onGet(`${baseURL}joshua`).networkError()
      AxiosCalls.awaitGet('joshua').then({}).catch(error => {
        expect(error).toEqual({error: 500, message: 'Connection Error'})
      })
    })

    it('delete request should return response', () => {
      mock.onDelete(`${baseUrl}/joshua`).reply(200, 'joshua')
      AxiosCalls.delete('joshua', {name: 'joshua'}).then((response) => {
        expect(response).toBeDefined()
      })
    })

    it('delete request should return error.response', () => {
      mock.onDelete(`${baseUrl}/joshua`).reply(400, 'joshua')
      AxiosCalls.delete('joshua', {name: 'joshua'}).then((response) => {}).catch((error) => {
        expect(error).toBeDefined()
      })
    })

    it('post request should return response', () => {
      mock.onPost(`${baseUrl}/joshua`).reply(200, 'joshua')
      AxiosCalls.post('joshua', {name: 'joshua'}, 'joshua').then((response) => {
        expect(response).toBeDefined()
      })
    })

    it('post request should return error response', () => {
      mock.onPost(`${baseUrl}/joshua`).reply(400, 'joshua')
      AxiosCalls.post('joshua', {name: 'joshua'}, 'joshua').then({}).catch((error) => {
        expect(error).toBeDefined()
      })
    })

    it('postTest should return error', () => {
      mock.onPost(`${baseUrl}/joshua`).reply(400)
      AxiosCalls.postTest('joshua', {data: 'joshua'}, 'felix').then(() => {})
        .catch((error) => {
          expect(error).toBeDefined()
        })
    })

    it('postTest should return response', () => {
      mock.onPost(`${baseUrl}/joshua`).reply(200)
      AxiosCalls.postTest('joshua', { data: 'joshua' }, 'felix').then((response) => {
        expect(response).toBeDefined()
      })
    })
  })
})
