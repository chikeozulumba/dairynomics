import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import router from '@/router/router'
import LostPinModule from '@/store/modules/LostPinModule'
jest.mock('@/router/router')

const commit = jest.fn()
router.push = jest.fn()

const mock = new MockAdapter(axios)
const state = {
  user_id: 2,
  forgotpin: '',
  verification_code: '',
  loading: false
}

describe('testing the lost pin module', () => {
  afterEach(() => {
    // wrapper = shallow(UserAuthSignUp)
    mock.reset()
  })
  it('test the getter in in the store', async () => {
    const state = {
      user_id: '',
      forgotpin: '',
      verification_code: '',
      loading: false,
      error: ''
    }
    LostPinModule.getters.getForgotPin(state)
  })
  it('test send code action', () => {
    const commit = jest.fn()
    const resp = {
      country_code: '+254',
      phone: ''
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/send_verification_code').reply(201, resp)
    LostPinModule.actions.sendVerification({ commit }, resp)
  })

  it('calls the forgotPinMutator mutation when the msg is equla to state.forgotpin', () => {
    const newState = {...state, forgotpin: 'joshua'}
    LostPinModule.mutations.forgotPinMutator(newState, 'david')
    expect(newState.forgotpin).toEqual('david')
  })

  it('calls the forgotPinMutator mutation when the msg is equla to state.forgotpin', () => {
    const newState = {...state, forgotpin: 'joshua'}
    LostPinModule.mutations.forgotPinMutator(newState, 'joshua')
    expect(newState.forgotpin).toEqual('joshua')
  })

  it('calls the loader mutation ', () => {
    LostPinModule.mutations.loader(state, true)
    expect(state.loading).toEqual(true)
  })

  it('calls the error mutation ', () => {
    LostPinModule.mutations.error(state)
    expect(state.error).toEqual('Something went wrong, please try again!')
  })

  it('calls the updateVerificationCode mutation ', () => {
    LostPinModule.mutations.updateVerificationCode(state, [24, 4])
    expect(state.verification_code).toEqual(24)
    expect(state.user_id).toEqual(4)
  })

  it('getForgotPin getters should return state', () => {
    const expectedResponse = LostPinModule.getters.getForgotPin(state)
    expect(expectedResponse).toEqual(state)
  })

  it('sendVerification action should call commit when the axios is successful', () => {
    const response = {
      success: true,
      verification_code: 23,
      user: {
        id: 4
      }
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/send_verification_code').reply(200, response)

    LostPinModule.actions.sendVerification({ commit }, { no: 34 })
      .then(() => {
        expect(commit).toHaveBeenCalled()
      })
  })

  it('sendVerification action should call commit with error when the axios is not successful', () => {
    const response = {
      success: false
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/send_verification_code').reply(200, response)

    LostPinModule.actions.sendVerification({ commit }, { no: 34 })
      .then(() => {
        expect(commit).toHaveBeenCalledWith('error')
      })
  })

  it('sendVerification action should call commit with error when the axios is not successful with error statuscode', () => {
    const response = {
      success: false
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/send_verification_code').reply(404, response)

    LostPinModule.actions.sendVerification({ commit }, { no: 34 })
      .then(() => {
        expect(commit).toHaveBeenCalledWith('error')
      })
  })

  it('loadSendConfirmationForm should call commit', () => {
    LostPinModule.actions.loadSendConfirmationForm({commit})
    expect(commit).toHaveBeenCalledWith('forgotPinMutator', 'lostpin')
  })

  it('loadChangePinForm should call commit', () => {
    LostPinModule.actions.loadChangePinForm({commit})
    expect(commit).toHaveBeenCalledWith('forgotPinMutator', 'accountCode')
  })

  it('accountCode should call router.push with profile as the arguments', () => {
    const response = {
      success: true,
      user: {
        country_code: ['+', 345]
      }
    }
    const state = {
      user_id: '6'
    }
    mock.onPut('https://beta.cowsoko.com/api/v1/change_password/6').reply(200, response)
    LostPinModule.actions.accountCode({ state, commit }, 4509)
      .then(() => {
        expect(router.push).toHaveBeenCalledWith('/profile')
      })
  })

  it('accountCode should call router.push with profile as the arguments', () => {
    const response = {
      success: true,
      user: {
        country_code: [345]
      }
    }
    const state = {
      user_id: 4
    }
    mock.onPut('https://beta.cowsoko.com/api/v1/change_password/4').reply(200, response)
    LostPinModule.actions.accountCode({ state, commit }, 4509)
      .then(() => {
        expect(router.push).toHaveBeenCalledWith('/profile')
      })
  })

  it('accountCode should call commit error when success is false with 200 statuscode', () => {
    const response = {
      success: false,
      user: {
        country_code: ['+', 345]
      }
    }
    const state = {
      user_id: 4
    }
    mock.onPut('https://beta.cowsoko.com/api/v1/change_password/4').reply(200, response)
    LostPinModule.actions.accountCode({ state, commit }, 4509)
      .then(() => {
        expect(commit).toHaveBeenCalledWith('error')
      })
  })

  it('accountCode should call commit error when success is false with statuscode that is error', () => {
    const response = {
      success: false
    }
    mock.onPut('https://beta.cowsoko.com/api/v1/change_password/4').reply(400, response)
    router.push = jest.fn()
    LostPinModule.actions.accountCode({ state, commit }, 4509)
      .then(() => {
        expect(commit).toHaveBeenCalledWith('error')
      })
  })
})
