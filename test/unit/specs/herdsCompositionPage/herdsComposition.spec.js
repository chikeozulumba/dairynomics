import { shallowMount } from '@vue/test-utils'
import HerdsComposition from '@/pages/HerdsCompositionPage/HerdsComposition.vue'
import AppButton from '@/components/ui/AppButton.vue'
import IntegerInput from '@/components/ui/IntegerInput.vue'

describe('HerdsComposition.vue', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(HerdsComposition, {
      propsData: {
        numberOfCows: 10,
        numberOfMilked: 0
      }
    })
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.findAll(IntegerInput)).toHaveLength(4)
    expect(wrapper.findAll(AppButton)).toHaveLength(1)
  })

  it('has recieved "numberOfCows" property', () => {
    expect(wrapper.vm.numberOfCows).toEqual(10)
  })

  it('should not display error, disable button and be unable to submit on initial rendering', () => {
    expect(wrapper.find('.error-message').text()).toEqual('')
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error, disable button and be unable to submit when an input value is empty', () => {
    wrapper.setData({ numberOfMilked: '' })
    expect(wrapper.find('.error-message').text())
      .toEqual('All inputs are required')
    wrapper.findAll(IntegerInput).at(0).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error, disable button and be unable to submit when input values do not sum up to required value', () => {
    wrapper.setProps({ numberOfCows: 15 })
    wrapper.setData({ numberOfMilked: 5 })
    expect(wrapper.find('.error-message').text())
      .toEqual('Inputs should sum up to 15')
    wrapper.findAll(IntegerInput).at(0).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should not display any error, should enable button and be able to submit when input values sum up to the required value', () => {
    wrapper.setProps({ numberOfCows: 20 })
    wrapper.setData({ numberOfDrys: 5 })
    wrapper.setData({ numberOfMilked: 5 })
    wrapper.setData({ numberOfHeifers: 5 })
    wrapper.setData({ numberOfCalves: 5 })

    expect(wrapper.find('.error-message').text()).toEqual('')
    wrapper.findAll(IntegerInput).at(0).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeTruthy()
    expect(wrapper.emitted().submit).toHaveLength(1)
    expect(wrapper.emitted().submit[0]).toEqual([{ numberOfMilked: 5 }])
    expect(wrapper.find(AppButton).attributes('disabled')).toBeUndefined()
  })

  it('should emit "submit" event', () => {
    wrapper.find(AppButton).vm.$emit('click')
    expect(wrapper.emitted().submit).toBeTruthy()
    expect(wrapper.emitted().submit).toHaveLength(2)
  })

  it('should emit "changeState" event', () => {
    wrapper.find('a').trigger('click')
    expect(wrapper.emitted().changeStage).toBeTruthy()
    expect(wrapper.emitted().changeStage).toHaveLength(1)
  })
})
