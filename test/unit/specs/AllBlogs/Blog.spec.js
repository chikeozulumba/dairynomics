import { shallowMount } from '@vue/test-utils'
import Blog from '@/pages/Blog/Blog'

describe('Blog', () => {
  const wrapper = shallowMount(Blog, {
    currentUrl: 'http://localhost/blogs/23',
    propsData: {
      title: 'This is a blog title',
      likes: 23,
      comments: 9,
      image: 'http://localhost/assets/image/noimage.jpg',
      url: 'http://localhost/blogs/23'
    }
  })

  it('it should render the required html', () => {
    expect(wrapper.contains('span')).toBe(true)
    expect(wrapper.contains('img')).toBe(true)
    expect(wrapper.contains('h4')).toBe(true)
    expect(wrapper.contains('div')).toBe(true)
  })
})
