import { shallowMount, createLocalVue } from '@vue/test-utils'
import BlogPage from '@/pages/Blog'
import Vuex from 'vuex'
import Router from 'vue-router'

jest.mock('@/router/router')
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
    path: '/'
  }]
})
const getters = {
  getBlogs: () => ({
    isLoading: false,
    blogs: {
      links: {
        prev: true,
        next: false
      },
      data: {
        blogs: 'joshua'
      }
    }
  })
}

const actions = {
  getAllBlogs: jest.fn()
}

const store = new Vuex.Store({
  actions,
  getters
})

const wrapper = shallowMount(BlogPage, {localVue, store, router})

wrapper.vm.$router.push = jest.fn()

describe('Blog Page Test', () => {
  it('should render when all the data values are initialized', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })

  it('gotoPrevious page should call this.paginatedMethod and this.getAllBlogs', () => {
    const previousPageSpy = jest.spyOn(wrapper.vm, 'goToPreviousPage')
    previousPageSpy()
    expect(actions.getAllBlogs).toHaveBeenCalled()
  })

  it('gotoNext page should call this.paginatedMethod and this.getAllBlogs', () => {
    const nextPageSpy = jest.spyOn(wrapper.vm, 'goToNextPage')
    nextPageSpy()
    expect(wrapper.vm.$router.push).toHaveBeenCalled()
    expect(actions.getAllBlogs).toHaveBeenCalled()
  })
})
