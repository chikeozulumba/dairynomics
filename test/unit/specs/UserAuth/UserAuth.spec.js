import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserAuth from '@/components/ui/userAuth/UserAuth'
import Vue from 'vue'
import { eventBus } from '@/utils/helperFunctions'

const EventBus = new Vue()

const GlobalPlugins = {
  install (v) {
    // Event bus
    v.prototype.$bus = EventBus
  }
}

// create a local instance of the global bus
const localVue = createLocalVue()
localVue.use(GlobalPlugins)

describe('UserAuth', () => {
  let wrapper

  test('should trigger the closeAuthModal method', () => {
    wrapper = shallowMount(UserAuth)
    const spy = jest.spyOn(wrapper.vm, 'closeAuthModal')
    wrapper.setData({
      showModal: false,
      form: 'signup'
    })
    wrapper.vm.closeAuthModal()
    expect(spy).toHaveBeenCalled()
  })

  it('eventBus bus on created should show form', () => {
    eventBus.$emit('showModal', 'signup')
    expect(wrapper.vm.form).toEqual('signup')
  })

  it('eventBus bus on created should show form', () => {
    eventBus.$emit('closeModal')
    expect(wrapper.vm.showModal).toEqual(false)
  })
})
