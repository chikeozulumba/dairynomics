import { shallowMount } from '@vue/test-utils'
import RevenueProjection from '@/pages/FarmStatsPage/RevenueProjection'

jest.mock('@/mixins/isLoggedInMixin')
describe('HerdsCompositionPage', () => {
  const wrapper = shallowMount(RevenueProjection, {
    propsData: {
      milkQuantity: 25,
      milkPrice: 12,
      numberOfCows: 10
    }
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('renders 5 "list" element', () => {
    expect(wrapper.findAll('li').length).toEqual(5)
  })
})
