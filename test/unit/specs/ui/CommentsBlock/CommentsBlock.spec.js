import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import CommentsBlock from '@/components/ui/CommentsBlock/CommentsBlock.vue'
import moment from 'vue-moment'

let wrapper
const localVue = createLocalVue()
localVue.use(moment)

describe('@components/ui/CommentsBlock/CommentsBlock.vue', () => {
  beforeEach(() => {
    wrapper = mount(CommentsBlock, {
      localVue,
      propsData: {
        comments: [
          {
            comment: 'Comment',
            created_at: {
              date: '2019-06-04 16:39:01.000000',
              timezone_type: 3,
              timezone: 'Africa/Nairobi'
            },
            created_by: {
              id: '1',
              first_name: 'First Name',
              last_name: 'Last Name'
            },
            id: '2',
            likes: 0,
            number_of_replies: 0,
            replies: [],
            updated_at: {
              date: '2019-06-04 16:39:01.000000',
              timezone_type: 3,
              timezone: 'Africa/Nairobi'
            },
            user_likes: []
          }
        ],
        meta: {
          current_page: 2,
          from: 1,
          last_page: 4,
          path: '',
          per_page: 10,
          to: 3,
          total: 4
        },
        userId: '1'
      }
    })
  })

  it('should render with default props', () => {
    wrapper = shallowMount(CommentsBlock, {
      localVue
    })
    expect(wrapper.props().comments).toEqual([])
  })

  it('contains the `next button`', () => {
    expect(wrapper.contains('.btn-next')).toBe(true)
  })

  it('emits a "click" property when `next button` is clicked', () => {
    wrapper.find('.btn-next').trigger('click')
    expect(wrapper.emitted('loadPage')).toBeTruthy()
  })

  it('emits a "click" property when `like button` is clicked', () => {
    wrapper.find('.btn-like').trigger('click')
    expect(wrapper.emitted('likeComment')).toBeTruthy()
  })

  it('emits a "click" property when `delete button` is clicked', () => {
    wrapper.find('.btn-delete').trigger('click')
    const onDelete = jest.fn()
    wrapper.vm.$children[0].onDelete = onDelete
    wrapper.find('.btn-delete').trigger('click')
    expect(onDelete).toHaveBeenCalled()
  })

  it('emits a "click" property when `post button` is clicked', () => {
    wrapper.vm.$children[0].editing = true
    wrapper.vm.$children[0].$children[0].body = 'comment'
    wrapper.find('.post-button').trigger('click')
    expect(wrapper.emitted('editComment')).toBeTruthy()
  })

  it('emits a "click" property when `post button` is clicked', () => {
    wrapper.vm.$children[0].replying = true
    wrapper.vm.$children[0].$children[0].body = 'comment'
    wrapper.find('.post-button').trigger('click')
    expect(wrapper.emitted('submitReply')).toBeTruthy()
  })

  it('onReply method', () => {
    const onReplySpy = jest.spyOn(wrapper.vm, 'onReply')
    onReplySpy(34)
    expect(wrapper.vm.commentId).toEqual(34)
  })

  it('submitComment', () => {
    const submitCommentSpy = jest.spyOn(wrapper.vm, 'submitComment')
    submitCommentSpy(null, 'joshua')
    expect(wrapper.emitted('submitComment')).toBeTruthy()
  })

  it('submitComment', () => {
    wrapper.vm.commentBody = 'joshua'
    const submitCommentSpy = jest.spyOn(wrapper.vm, 'submitComment')
    submitCommentSpy('fredrick', null)
    expect(wrapper.emitted('submitReply')).toBeTruthy()
  })

  it('submitComment', () => {
    wrapper.vm.commentBody = 'joshua'
    const submitCommentSpy = jest.spyOn(wrapper.vm, 'submitComment')
    submitCommentSpy(null, null)
    expect(wrapper.emitted('submitComment')).toBeTruthy()
  })

  it('submitComment', () => {
    const submitCommentSpy = jest.spyOn(wrapper.vm, 'submitComment')
    submitCommentSpy(23, 'joshua')
    expect(wrapper.emitted('submitReply')).toBeTruthy()
  })

  it('submitComment', () => {
    const submitCommentSpy = jest.spyOn(wrapper.vm, 'submitComment')
    submitCommentSpy(23, 'joshua')
    expect(wrapper.emitted('submitReply')).toBeTruthy()
  })

  it('deleteComment', () => {
    const deleteSpy = jest.spyOn(wrapper.vm, 'deleteComment')
    deleteSpy('joshua', 'yes')
    expect(wrapper.emitted('deleteComment')).toBeTruthy()
  })
})
