import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import LiveStockExpert from '@/components/ui/LiveStockExpert.vue'
import Router from 'vue-router'
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

const propsData = {
  id: '4',
  expert_names: 'joshua',
  field: 'joshua',
  pic: 'https://joshua.jpg',
  county: 'joshua'
}
const wrapper = shallowMount(LiveStockExpert, { localVue, propsData })

describe('LiveStock.vue test', () => {
  it('should render livestock.vue', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('computed should call parseInt', () => {
    expect(wrapper.vm.expert_id).toBe(4)
  })
})
