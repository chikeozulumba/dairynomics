import { shallowMount, RouterLinkStub } from '@vue/test-utils'
import CowsCount from '@/components/ui/CowsCount.vue'
import AppButton from '@/components/ui/AppButton.vue'
import IntegerInput from '@/components/ui/IntegerInput.vue'

describe('CowsCount.vue', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(CowsCount, {
      propsData: {
        numberOfCows: 0,
        title: 'title',
        heading: 'heading',
        routerNote: 'routerNote'
      },
      stubs: { 'router-link': RouterLinkStub }
    })
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.findAll(IntegerInput)).toHaveLength(1)
    expect(wrapper.findAll(AppButton)).toHaveLength(1)
  })

  it('has recieved "numberOfCows" property', () => {
    expect(wrapper.vm.numberOfCows).toEqual(0)
    expect(wrapper.vm.cowsCount).toEqual(0)
  })

  it('should not display error message, disable button and be unable to submit on initial rendering', () => {
    expect(wrapper.find('.error-message').text()).toEqual('')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error, disable button and be unable to submit when input value is empty', () => {
    wrapper.setData({ cowsCount: '' })
    expect(wrapper.find('.error-message').text())
      .toEqual('Number of cows is required')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error, disable button and be unable to submit when input value is "zero"', () => {
    wrapper.setData({ cowsCount: 0 })
    expect(wrapper.find('.error-message').text())
      .toEqual('Number of cows should be greater than 0')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should not display error, should enable button and be able to submit when input has a positive value', () => {
    wrapper.setData({ cowsCount: 6 })
    expect(wrapper.find('.error-message').text()).toEqual('')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeTruthy()
    expect(wrapper.emitted().submit).toHaveLength(1)
    expect(wrapper.emitted().submit[0]).toEqual([{ numberOfCows: 6 }])
    expect(wrapper.find(AppButton).attributes('disabled')).toBeUndefined()
  })

  it('should emit "submit" event', () => {
    wrapper.setData({ cowsCount: 10 })
    wrapper.find(AppButton).vm.$emit('click')
    expect(wrapper.emitted().submit).toBeTruthy()
    expect(wrapper.emitted().submit).toHaveLength(2)
    expect(wrapper.emitted().submit[1]).toEqual([{numberOfCows: 10}])
  })
})
