import {createLocalVue, shallowMount} from '@vue/test-utils'
import SelectCountry from '@/components/ui/SelectCountry'

const localVue = createLocalVue()

const propsData = {
  options: []
}

const wrapper = shallowMount(SelectCountry, {localVue, propsData}

)
describe('SelectCountry test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
