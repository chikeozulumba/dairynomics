import { shallowMount } from '@vue/test-utils'
import EcommerceSideBar from '@/components/ui/EcommerceSideBar.vue'
import EcommerceSideBarLayout from '@/components/layout/EcommerceSideBarLayout.vue'
import MainSwitcher from '@/components/layout/MainSwitcher.vue'

describe('EcommerceSideBarLayout Test', () => {
  const wrapper = shallowMount(EcommerceSideBarLayout)

  it('renders the Layout component on page load', () => {
    expect(wrapper.find(EcommerceSideBar).exists()).toBe(true)
  })

  it('renders the mainswitcher component on page load', () => {
    expect(wrapper.find(MainSwitcher).exists()).toBe(true)
  })
})
