import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuelidate from 'vuelidate'
import ImageCarousel from '@/components/ui/ImageCarousel.vue'

const localVue = createLocalVue()

localVue.use(Vuelidate)

describe('ImageCarousel', () => {
  let wrapper

  it('currentNumber should not be changed when currentNumber equal length of images', () => {
    wrapper = shallowMount(ImageCarousel, {
      propsData: {
        productImages: ['https://cdn.pixabay.com/photo/2017/08/04/09/39/indian-cow-2579534_1280.jpg']
      }
    }
    )
    wrapper.setData({ images: ['https://cdn.pixabay.com/photo/2017/08/04/09/39/indian-cow-2579534_1280.jpg', 'http://i.imgur.com/PUD9HQL.jpg'],
      currentNumber: 0})
    wrapper.vm.next()
    expect(wrapper.vm.currentNumber).toBe(1)
    wrapper.vm.next()
    expect(wrapper.vm.currentNumber).toBe(1)
  })

  it('should trigger the prev method', () => {
    wrapper = shallowMount(ImageCarousel, {
      propsData: {
        productImages: ['https://cdn.pixabay.com/photo/2017/08/04/09/39/indian-cow-2579534_1280.jpg']
      }
    }
    )
    wrapper.setData({currentNumber: 1})
    wrapper.vm.prev()
    expect(wrapper.vm.currentNumber).toBe(0)
    wrapper.setData({currentNumber: 0})
    wrapper.vm.prev()
    expect(wrapper.vm.currentNumber).toBe(0)
  })
})
