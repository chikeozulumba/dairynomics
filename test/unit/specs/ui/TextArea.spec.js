import { shallowMount } from '@vue/test-utils'
import TextArea from '@/components/ui/TextArea.vue'

describe('@components/ui/TextArea.vue', () => {
  const wrapper = shallowMount(TextArea, { propsData: {
    className: 'my-text-area'
  }})
  it('emits an "input" event when text is input into the text area', () => {
    wrapper.find('.my-text-area').trigger('input')
    expect(wrapper.emitted('input').length).toEqual(1)
  })
})
