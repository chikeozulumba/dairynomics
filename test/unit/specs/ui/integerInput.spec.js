import { mount, shallowMount } from '@vue/test-utils'
import IntegerInput from '@/components/ui/IntegerInput.vue'

describe('IntegerInput.vue', () => {
  let wrapper

  beforeAll(() => {
    wrapper = shallowMount(IntegerInput, {
      propsData: { value: 0 }
    })
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('it should remove zeros that starts numbers', () => {
    const input = wrapper.find('input')
    input.element.value = '06'
    input.trigger('input')
    expect(input.element.value).toEqual('6')
  })

  it('it should replace non-numberic values with empty string', () => {
    const input = wrapper.find('input')
    input.element.value = 'm'
    input.trigger('input')
    expect(input.element.value).toEqual('')
  })

  it('should focus the input component', () => {
    wrapper = mount(IntegerInput, {
      propsData: { value: 0, autofocus: true }
    })

    expect(wrapper.find('input').element).toBe(document.activeElement)
  })
})
