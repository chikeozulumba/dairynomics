import PostCommentModule from '@/store/modules/PostCommentModule'
import { mock as mocked } from '@/store/modules/mocks/mockInstance'

const commit = jest.fn()

Storage.prototype.getItem = jest.fn(() => 24)

describe('PostCommentModule module', () => {
  let comment = 'hello'
  let userId = 1886
  const data = {
    comment: comment, user_id: userId
  }
  let state = {
    blog: {},
    blog_id: 13,
    comments: [{comment, user_likes: [], likes: 0}],
    comment_reply_id: '',
    reply_id: '',
    comment_relpy: '',
    error: '',
    comment_id: 12,
    active_like: true,
    userIds: {},
    likeBlogUserIds: []
  }

  it('test the getter in in the store', async () => {
    const state = { blog: {} }
    PostCommentModule.getters.getBlogInfo(state)
  })

  it('test the setBlogIds mutation', () => {
    const state = { blogIds: [] }
    const blogIds = [15, 20, 30, 43]
    PostCommentModule.mutations.setBlogIds(state, blogIds)
  })

  it('test the setBlogsPerPage mutation', () => {
    const state = { blogsPerPage: 0 }
    const blogsPerPage = 10
    PostCommentModule.mutations.setBlogsPerPage(state, blogsPerPage)
  })

  it('test the likeSingleComment mutation', () => {
    const state = { comments: [{id: 700, likes: 0, user_likes: []}] }
    const commentId = { id: 700 }
    PostCommentModule.mutations.likeSingleComment(state, commentId)
  })

  it('test the unlikeSingleComment mutation', () => {
    const userId = localStorage.getItem('userId')
    const state = { comments: [{id: 700, likes: 1, user_likes: [userId]}] }
    const commentId = { id: 700 }
    PostCommentModule.mutations.unlikeSingleComment(state, commentId)
  })

  it('test the updateCommentId mutation', () => {
    const replyId = 12
    PostCommentModule.mutations.updateCommentId(state, replyId)
  })

  it('test the  setBlog mutation', () => {
    const blog = {}
    PostCommentModule.mutations.setBlog(state, blog)
  })

  it('test the  updateComments mutation', () => {
    const comment = []
    PostCommentModule.mutations.updateComments(state, comment)
  })

  it('test the updateCommentReplyId mutation', () => {
    const reply = []
    PostCommentModule.mutations.updateCommentReplyId(state, reply)
  })

  it('test the setComments mutation', () => {
    const comment = []
    PostCommentModule.mutations.setComments(state, comment)
  })

  it('test the setError mutation', () => {
    const errors = 'Sorry something went wrong, please try again!'
    PostCommentModule.mutations.setError(state, errors)
  })

  it('test the setBlogId mutation', () => {
    const blogId = 13
    PostCommentModule.mutations.setBlogId(state, blogId)
  })

  it('test the setUserIds mutation', () => {
    const userId = 13
    PostCommentModule.mutations.setUserIds(state, userId)
  })

  it('test deleteReply mutation', () => {
    const userId = 13
    const state = {
      comment_id: 1,
      comments: [{id: 1, replies: [{id: 1, comment: [{id: 13, comment: 'its'}]}]}]
    }
    PostCommentModule.mutations.deleteReply(state, userId)
  })

  it('test editReplies mutation', () => {
    const newReply = 'this is cool'
    const state = {
      reply_id: 1,
      comment_id: 1,
      comments: [{id: 1, replies: [{id: 1, comment: [{id: 13, comment: 'its'}]}]}]
    }
    PostCommentModule.mutations.editComment(state, newReply)
  })

  it('test addReplies mutation', () => {
    const reply = 'this is cool'
    const state = {
      reply_id: 1,
      comment_id: 1,
      comments: [{id: 1, replies: [{id: 1, comment: [{id: 13, comment: 'its'}]}]}]
    }
    PostCommentModule.mutations.addReplies(state, reply)
  })

  it('test updateReplyLikes mutation', () => {
    let payload = {userId: 4, replyId: 7}
    const state = {
      userIds: {},
      reply_id: 1,
      comment_id: 1,
      comments: [{id: 1, replies: [{id: 7, likes: 12, comment: [{id: 13, comment: 'its'}]}]}]
    }
    PostCommentModule.mutations.updateReplyLikes(state, payload)
  })

  it('test updateReplyUnLikes mutation', () => {
    let payload = {userId: 4, replyId: 7}
    const userId = localStorage.getItem('userId')
    const state = {
      userIds: {},
      reply_id: 1,
      comment_id: 1,
      comments: [
        {
          id: 1,
          replies: [{
            user_likes: [userId],
            id: 7,
            likes: 12,
            comment: [
              {
                id: 13,
                comment: 'its'
              }
            ]
          }]
        }
      ]
    }
    PostCommentModule.mutations.updateReplyUnLikes(state, payload)
  })

  it('test updateBlogLikes mutation', () => {
    let userId = 12
    const state = {
      likeBlogUserIds: [],
      blog: {likes: 12}
    }
    PostCommentModule.mutations.updateBlogLikes(state, userId)
  })

  it('test updateBlogLikes mutation', () => {
    let userId = 12
    const state = {
      likeBlogUserIds: [],
      blog: {likes: 12}
    }
    PostCommentModule.mutations.removeUserIdFromBlogLikes(state, userId)
  })

  it('test deleteComment mutation', () => {
    const commentId = 13
    const state = {
      comment_id: 1,
      comments: [{comment: 'its good', id: 12}]
    }
    PostCommentModule.mutations.deleteComment(state, commentId)
  })

  it('test the setLikeBlogUserIds mutation', () => {
    const blogUserId = [13]
    PostCommentModule.mutations.setLikeBlogUserIds(state, blogUserId)
  })

  it('test the removeReply mutation', () => {
    const delReply = 'that was cool'
    PostCommentModule.mutations.deleteReply(state, delReply)
  })

  it('test PostCommentModule action', async () => {
    let MockUpdateCommentsMutation = (state, comment) => {
      state.comments.push(comment)
    }
    mocked.onPost(`https://beta.cowsoko.com/api/v1/blogs/13/comments`).reply(201, state)
    await PostCommentModule.actions.postComment({MockUpdateCommentsMutation, state}, data)
  })

  it('test editCommentAction action', async () => {
    let editReplies = jest.fn()
    mocked.onPut(`https://beta.cowsoko.com/api/v1/comments/${state.reply_id}`).reply(201, data)
    await PostCommentModule.actions.editCommentAction({editReplies, state}, data)
  })

  it('test postReply action', async () => {
    let addReplies = jest.fn()
    mocked.onPost(`https://beta.cowsoko.com/api/v1/comments/${state.comment_reply_id}/reply`).reply(201, data)
    await PostCommentModule.actions.postReply({addReplies, state}, data)
  })

  it('test likeSingleReply action', async () => {
    const comId = 12
    const commit = jest.fn()
    mocked.onPut(`https://beta.cowsoko.com/api/v1/comments/${comId}/like`).reply(200, data)
    await PostCommentModule.actions.likeSingleReply({commit})
  })
  // editCommentAction
  it('test unlikeSingleReply action', async () => {
    const commit = jest.fn()
    const comId = 12
    mocked.onPut(`https://beta.cowsoko.com/api/v1/comments/${comId}/like`).reply(200, data)
    await PostCommentModule.actions.unlikeSingleReply({commit})
  })

  it('test likeSingleComment action', async () => {
    const commentId = 1
    const commit = jest.fn()
    mocked.onPut(`https://beta.cowsoko.com/api/v1/comments/${commentId}/like`).reply(200, data)
    await PostCommentModule.actions.likeSingleComment({commit}, commentId)
  })

  it('test unLikeSingleComment action', async () => {
    const commentId = 1
    const commit = jest.fn()
    mocked.onPut(`https://beta.cowsoko.com/api/v1/comments/${commentId}/unlike`).reply(200, data)
    await PostCommentModule.actions.unLikeSingleComment({commit}, commentId)
  })

  it('test editCommentAction action', async () => {
    const comment = 'this is cool'
    let commit = jest.fn()
    let state = {comment_id: 1}
    mocked.onPut(`https://beta.cowsoko.com/api/v1/comments/90`).reply(200, data)
    await PostCommentModule.actions.editCommentAction({commit, state}, comment)
  })

  it('test getReplyLikes action', async () => {
    let commit = jest.fn()
    let replyId = 1
    let state = {comment_id: 1}
    mocked.onGet(`https://beta.cowsoko.com/api/v1/comments/${replyId}/likes`).reply(202, data)
    await PostCommentModule.actions.getReplyLikes({commit, state})
  })

  it('test likeSingleBlog action', async () => {
    let commit = jest.fn()
    let blogId = 13
    mocked.onPut(`https://beta.cowsoko.com/api/v1/blogs/${blogId}/like`).reply(202, data)
    await PostCommentModule.actions.likeSingleBlog({commit})
  })

  it('test getSingleBlog action', async () => {
    let commit = jest.fn()
    let blogId = 13
    const state = {blogIds: [13, 14, 15, 17, 43, 45]}
    const dispatch = jest.fn()
    mocked.onGet(`https://beta.cowsoko.com/api/v1/blogs/${blogId}`).reply(200, data)
    await PostCommentModule.actions.getSingleBlog({commit, state, dispatch}, {blogId})
    mocked.onGet(`https://beta.cowsoko.com/api/v1/blogs/${blogId}/comments`).reply(200, data)
  })

  it('test unLikeSingleBlog action', async () => {
    let commit = jest.fn()
    let blogId = 13
    mocked.onPut(`https://beta.cowsoko.com/api/v1/blogs/${blogId}/unlike`).reply(202, data)
    await PostCommentModule.actions.unLikeSingleBlog({commit})
  })

  it('test getBlogLikes action', async () => {
    let commit = jest.fn()
    let state = {blogId: 13}
    mocked.onGet(`https://beta.cowsoko.com/api/v1/blogs/${state.blogId}/likes`).reply(202, data)
    await PostCommentModule.actions.getBlogLikes({commit, state})
  })
  // removeReply

  it('test removeReply action', async () => {
    let commit = jest.fn()
    let state = {
      comment_id: 1
    }
    mocked.onDelete(`https://beta.cowsoko.com/api/v1/comments/${state.comment_id}`).reply(202, data)
    await PostCommentModule.actions.removeComment({commit, state})
  })

  it('getAllBlogs', () => {
    const response = {
      meta: {
        per_page: 'joshua'
      },
      data: {
        blogs: [{
          id: 34
        }]
      }
    }
    mocked.onGet('https://beta.cowsoko.com/api/v1/blogs?page=4').reply(200, response)
    const state = {
      loadingState: true,
      blogsIds: [1, 3, 5]
    }
    PostCommentModule.actions.getAllBlogs({commit, state}, {pageNumber: 4}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })
})
