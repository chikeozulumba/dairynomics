import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import IndividualProducts from '@/pages/IndividualProduct/IndividualProducts'

const localVue = createLocalVue()
localVue.use(Vuex)

const getters = {
  getIndividualProduct: () => {
    return {
      photos: 'https://joshua.jpg'
    }
  }
}
const store = new Vuex.Store({getters
})

const wrapper = shallowMount(IndividualProducts, {store, localVue})

describe('individualProducts', () => {
  it('should render the IndividualProducts component', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('getProductImages computed', () => {
    expect(wrapper.vm.getProductImages).toEqual('https://joshua.jpg')
  })

  it('getProduct Images watch', () => {
    wrapper.vm.$options.watch.getProductImages.call(wrapper.vm, 'joshua.jpeg')
    expect(wrapper.vm.productImages).toEqual('joshua.jpeg')
  })
})
