import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserCard from '@/pages/Message/UserCard'
import Vuex from 'vuex'

let getters
let localVue = createLocalVue()
localVue.use(Vuex)

const mockUsers = [{
  'id': '3564',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}, {
  'id': '3355',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}]

describe('UserCard', () => {
  let wrapper
  beforeAll(() => {
    getters = {
      chatMate: () => mockUsers[0]
    }
    wrapper = shallowMount(UserCard, {
      propsData: {
        user: mockUsers[0],
        toggleSideBar: jest.fn()
      },
      localVue,
      store: new Vuex.Store({ getters })
    })
  })

  it('renders without error', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('calls switchActiveChatMate and not switch user when user-card is clicked', () => {
    const handleSpy = jest.spyOn(wrapper.vm, 'switchActiveChatMate')
    const stub = jest.fn()
    wrapper.vm.$on('switchActiveChatMate', stub)
    handleSpy()
    expect(stub).not.toHaveBeenCalled()
  })

  it('calls switchActiveChatMate and switch user when user-card is clicked', () => {
    wrapper.setProps({
      user: mockUsers[1]
    })
    const handleSpy = jest.spyOn(wrapper.vm, 'switchActiveChatMate')
    const switchActiveChatMateSpy = jest.fn()
    wrapper.vm.$on('switchActiveChatMate', switchActiveChatMateSpy)
    const showUnsentMessageIconSpy = jest.fn()
    wrapper.vm.$on('switchActiveChatMate', showUnsentMessageIconSpy)
    handleSpy()
    expect(showUnsentMessageIconSpy).toBeCalled()
    expect(switchActiveChatMateSpy).toBeCalled()
  })

  it('should set is Active to false', () => {
    wrapper.setProps({
      user: { ...mockUsers, isActive: true }
    })
    expect(wrapper.vm.getIsActiveClasses).toBe('user-card is-active')
  })

  it('should render the appropriate status color', () => {
    wrapper.setProps({
      user: { ...mockUsers, status: 'Offline' }
    })
    expect(wrapper.vm.getStatusClasses).toBe('user-details-status offline')
  })
})
