import { shallowMount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import SideContent from '@/pages/Message/SideContent';
import Vuex from 'vuex';
import PeerModel from '@/utils/peerModel/index.js';

let getters
let actions
let wrapper
const localVue = createLocalVue()
const router = new VueRouter()
localVue.use(Vuex)
localVue.use(VueRouter)

Storage.prototype.getItem = jest.fn(() =>
  JSON.stringify({
    id: 5,
    unread_messages_count: 4,
    last_chat_date: new Date()
  })
)
const mockUsers = [
  {
    id: '3564',
    first_name: 'Kristoffer',
    last_name: 'Stoltenberg',
    email: 'kristofferstoltenberg1@testusers.cowsoko',
    country: 'Uganda',
    country_code: '+256',
    phone: '0714433011',
    account_type: '',
    county: 'Boma',
    profile: {
      pic: 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
      bio: null,
      social: {
        facebook: null,
        twitter: null
      }
    },
    verified_account: 1,
    is_seller: 0,
    is_expert: 0
  },
  {
    id: '3554',
    first_name: 'Chris',
    last_name: 'Stoneberg',
    email: 'kristofferstoltenberg1@testusers.cowsoko',
    country: 'Uganda',
    country_code: '+256',
    phone: '0714433011',
    account_type: '',
    county: 'Boma',
    profile: {
      pic: 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
      bio: null,
      social: {
        facebook: null,
        twitter: null
      }
    },
    verified_account: 1,
    is_seller: 0,
    is_expert: 0
  }
]

describe('SideContent', () => {
  beforeAll(() => {
    actions = {
      chatMateAction: jest.fn(),
      loadMessagesAction: jest.fn(),
      updateUsersAction: jest.fn(),
      toggleSideBarAction: jest.fn()
    }
    getters = {
      allMessages: () => {
        return {
          message:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          date: '25 October 2018, 12:35am',
          doReverse: false,
          status: true
        }
      },
      chatMate: () => mockUsers[0],
      peerModel: () => new PeerModel()
    }

    router.push(`/message/${mockUsers[0].id}`)

    wrapper = shallowMount(SideContent, {
      localVue,
      router,
      propsData: {
        toggleSideBar: jest.fn(),
        updatePageData: jest.fn(),
        // showUnsentMessageIcon: jest.fn(),
        users: [],
        allUsers: []
      },
      store: new Vuex.Store({ actions, getters })
    })
    wrapper.setData({
      users: []
    })
    wrapper.setProps({
      allUsers: []
    })
  })

  it('renders without an error', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should not set the clicked user as the active chat mate for invalid user id', () => {
    wrapper.setData({
      users: mockUsers
    })
    const highlightActiveChatMateSpy = jest.spyOn(
      wrapper.vm,
      'highlightActiveChatMate'
    )
    const switchActiveChatMateSpy = jest.spyOn(
      wrapper.vm,
      'switchActiveChatMate'
    )
    switchActiveChatMateSpy(-1)
    expect(wrapper.vm.switchActiveChatMate).toHaveBeenCalled()
    expect(highlightActiveChatMateSpy).not.toHaveBeenCalled()
  })

  it('should set the clicked user as the active chat mate for a valid user id', () => {
    wrapper.setData({
      users: mockUsers
    })
    const highlightActiveChatMateSpy = jest.spyOn(
      wrapper.vm,
      'highlightActiveChatMate'
    )
    const switchActiveChatMateSpy = jest.spyOn(
      wrapper.vm,
      'switchActiveChatMate'
    )
    switchActiveChatMateSpy(0)
    expect(wrapper.vm.switchActiveChatMate).toHaveBeenCalled()
    expect(highlightActiveChatMateSpy).toHaveBeenCalled()
  })

  it('should not find a user if search does not match', () => {
    wrapper.setData({
      search: 'something else'
    })
    const filterUsersSpy = jest.spyOn(wrapper.vm, 'filterUsers')
    filterUsersSpy()
    expect(wrapper.vm.searchedUsers).toHaveLength(0)
  })

  it('should find a user that matches the search', () => {
    wrapper.setProps({
      allUsers: mockUsers
    })
    wrapper.setData({
      search: 'Kristoffer'
    })
    const filterUsersSpy = jest.spyOn(wrapper.vm, 'filterUsers')
    filterUsersSpy()
    expect(wrapper.vm.searchedUsers).toHaveLength(1)
  })

  it('should set active chat on component mount', () => {
    const users = [mockUsers[0]]
    wrapper.setProps({
      allUsers: users
    })
    const getActiveUserIndexSpy = jest.spyOn(wrapper.vm, 'getActiveUserIndex')
    const setActiveChatMateSpy = jest.spyOn(wrapper.vm, 'setActiveChatMate')
    const switchActiveChatMateSpy = jest.spyOn(
      wrapper.vm,
      'switchActiveChatMate'
    )
    setActiveChatMateSpy(false, users)
    expect(getActiveUserIndexSpy).toHaveBeenCalled()
    expect(switchActiveChatMateSpy).toHaveBeenCalled()
  })

  it('should fail to set active chat mate', () => {
    wrapper.setProps({
      allUsers: [mockUsers[0]]
    })
    const getActiveUserIndexSpy = jest.spyOn(wrapper.vm, 'getActiveUserIndex')
    const setActiveChatMateSpy = jest.spyOn(wrapper.vm, 'setActiveChatMate')
    const switchActiveChatMateSpy = jest.spyOn(
      wrapper.vm,
      'switchActiveChatMate'
    )
    setActiveChatMateSpy(false, [mockUsers[1]])
    expect(getActiveUserIndexSpy).toHaveBeenCalled()
    expect(switchActiveChatMateSpy).not.toHaveBeenCalledWith([
      undefined,
      false
    ])
  })

  it('should update users whe allUsers changes', () => {
    wrapper.setProps({
      allUsers: []
    })
    wrapper.setData({
      users: []
    })
    expect(wrapper.vm.users).toHaveLength(0)
  })

  it('should return empty array for users', () => {
    wrapper.setProps({
      allUsers: [mockUsers[0]]
    })
    expect(wrapper.vm.allUsers).toHaveLength(1)
  })

  it('hide sidebar when the close botton is clicked', () => {
    wrapper.setProps({ toggleSideBar: jest.fn() })
    const hideSideBarSpy = jest.spyOn(wrapper.vm, 'hideSideBar')
    hideSideBarSpy()
    expect(wrapper.vm.hideSideBar).toBeCalled()
  })

  it('sends a new message when the submit botton is clicked', () => {
    const hideSideBarSpy = jest.spyOn(wrapper.vm, 'hideSideBar')
    hideSideBarSpy()
    expect(wrapper.vm.hideSideBar).toBeCalled()
  })
})
