import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import publication from '@/pages/Publications/SinglePublicationIndex.vue'
import SinglePublication from '@/pages/Publications/SinglePublication.vue'
import Router from 'vue-router'
const localVue = createLocalVue()
let wrapper
localVue.use(Vuex)
localVue.use(Router)
const store = new Vuex.Store({
  state: {
    publicationId: '',
    publication: {data: {title: 'mocked-title', download_count: 2, thumbnail: 'image.png'}},
    error: '',
    download: '',
    loading: false,
    isLoading: false
  },
  getters: {
    publicationGetter: state => state.publication,
    downloadGetter: state => state.download,
    downloadLoader: state => state.loading,
    downloadError: state => state.error,
    isLoading: state => state.isLoading
  }
})
describe('test index file', () => {
  beforeEach(() => {
    const router = new Router({ path: '/publications', push: {} })
    wrapper = shallowMount(publication, {localVue, store, router, propsData: {shareText: '', farmPerformance: {result: 'excellent'}}})
  })
  it('renders the SinglePublication component on load', () => {
    expect(wrapper.find(SinglePublication).exists()).toBe(true)
  })
  it('should redirect to featured publication page when featured publication is clicked', () => {
    wrapper.findAll('.pulication-list').trigger('click')
    expect(wrapper.vm.$route.params).toEqual({})
  })
})
describe('test indivual publication component', () => {
  // beforeEach(() => {
  //   const wrapper = shallowMount(SinglePublication, {localVue, store, propsData: {shareText: '', farmPerformance: {result: 'excellent'}}})
  // })
  it('should download a publication', () => {
    const router = new Router({ path: '/publications', push: {}, params: {} })
    wrapper = shallowMount(SinglePublication, {localVue,
      router,
      store,
      mocks: {
        $route: {
          params: { id: 2 }
        }
      }})
    wrapper.findAll('.contact-button').trigger('click')
    expect(wrapper.vm.downloadPub).toBeInstanceOf(Function)
  })
  it('should show featured publication when Other Similar Publications is clicked', () => {
    const router = new Router({ path: '/publications', push: {}, params: {id: 2} })
    wrapper = shallowMount(SinglePublication, {localVue,
      router,
      store,
      mocks: {
        $route: {
          params: { id: 2 }
        }
      }})
    const Download = {Download: '<html>spinner</html'}
    wrapper.vm.$options.watch.downloadLoader.call(Download, Download)
    wrapper.findAll('.other-experts').trigger('click')
    expect(wrapper.find('fa-spinner')).toEqual({'selector': 'fa-spinner'})
  })
  it('should show download on component load', () => {
    const router = new Router({ path: '/publications', push: {}, params: {id: 2} })
    wrapper = shallowMount(SinglePublication, {localVue,
      router,
      store,
      mocks: {
        $route: {
          params: { id: 2 }
        }
      }})
    wrapper.vm.$options.watch.downloadLoader.call(jest.fn())
    wrapper.findAll('.other-experts').trigger('click')
    expect(wrapper.findAll('.fa-file-download').length).toBe(1)
  })
})
