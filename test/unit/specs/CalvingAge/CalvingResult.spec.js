import { shallowMount } from '@vue/test-utils'
import SocialMediaShare from '@/components/layout/SocialMediaShare'
import CalvingResult from '@/pages/FirstCalvingPage/CalvingResult'

jest.mock('@/mixins/isLoggedInMixin')

describe('FarmPerformance', () => {
  it('should render correct message when age of first calving is between 24 and 26 month', () => {
    const wrapper = shallowMount(CalvingResult, {
      propsData: {
        ageOfCalving: 24
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.find('h3').text()).toEqual('Excellent!')
    expect(wrapper.find('.info-message').text())
      .toEqual('You\'re doing pretty well! But you can always do better.')
    expect(wrapper.findAll(SocialMediaShare).length).toBe(1)
  })

  it('should render correct message when age of first calving is between 26 to 28 month', () => {
    const wrapper = shallowMount(CalvingResult, {
      propsData: {
        ageOfCalving: 27
      }
    })

    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.find('h3').text())
      .toEqual('Er.. Ok')
    expect(wrapper.find('.info-message').text())
      .toEqual('You\'re doing Ok, but you can do way better!')
    expect(wrapper.findAll(SocialMediaShare).length).toBe(1)
  })

  it('should render correct message when age of calving is between 28 to 40 month ', () => {
    const wrapper = shallowMount(CalvingResult, {
      propsData: {
        ageOfCalving: 34
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.find('h3').text())
      .toEqual('Below Average')
    expect(wrapper.find('.info-message').text())
      .toEqual('Your farm can perform so much better!')
    expect(wrapper.findAll(SocialMediaShare).length).toBe(0)
  })
})
