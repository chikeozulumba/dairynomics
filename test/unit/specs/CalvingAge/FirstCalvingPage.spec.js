
import { shallowMount } from '@vue/test-utils'
import FirstCalvingPage from '@/pages/FirstCalvingPage'
import CalvingResult from '@/pages/FirstCalvingPage/CalvingResult'
import BaseCalving from '@/pages/FirstCalvingPage/BaseCalving'

describe('FirstCalvingPag', () => {
  let wrapper

  beforeAll(() => {
    wrapper = shallowMount(FirstCalvingPage)
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render "BaseCalving" only on initial rendering', () => {
    expect(wrapper.findAll(BaseCalving).length).toBe(1)
    expect(wrapper.findAll(CalvingResult).length).toBe(0)
  })

  it('should render "CalvingResult" only', () => {
    wrapper.find(BaseCalving).vm.$emit('submit', 10)
    expect(wrapper.findAll(BaseCalving).length).toBe(0)
    expect(wrapper.findAll(CalvingResult).length).toBe(1)
  })
  it('should render "BaseCalving" when switched', () => {
    wrapper.find(CalvingResult).vm.$emit('submit', 10)
    expect(wrapper.findAll(BaseCalving).length).toBe(1)
    expect(wrapper.findAll(CalvingResult).length).toBe(0)
  })
})
