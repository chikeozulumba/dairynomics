import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import SingleExpertUpdate from '@/pages/ExpertUpdate/SingleUpdate'
import router from '@/router/router'

jest.mock('@/store')

const mockAction = jest.fn()
let localVue = createLocalVue()
localVue.use(Vuex)

const updateData = {text: 'heehhr', updated_by: {first_name: 'sasa', profile: {pic: 'asass'}}, number_of_comments: 34}

describe('SingleUpdate', () => {
  let wrapper, store, getters, actions
  beforeEach(() => {
    getters = {
      getUpdate: () => updateData,
      getUpdateCreator: () => updateData,
      getSelectedReply: () => 23,
      getSelectedComment: () => 23,
      getComments: () => [{id: 23}, {id: 24}],
      UPDATES: () => [{id: 23}, {id: 24}, {id: 25}],
      getExpertProfileInformation: () => ({})
    }
    actions = {
      fetchUpdate: mockAction,
      fetchComments: mockAction,
      editUpdateComment: mockAction,
      likeAnUpdate: mockAction,
      likeUpdateComment: mockAction,
      unlikeUpdate: mockAction,
      unlikeUpdateComment: mockAction,
      editReplyToComment: mockAction,
      setUpdateCreatorId: mockAction,
      setUserId: mockAction,
      setReplyId: mockAction,
      unlikeAnUpdate: mockAction,
      likeUpdateReply: mockAction,
      setSelectedUpdate: mockAction,
      setCommentId: mockAction,
      commentOnUpdate: mockAction,
      fetchUpdateCreatorProfile: mockAction,
      deleteReply: mockAction,
      setSelected: mockAction,
      deleteAComment: mockAction,
      replytoComment: mockAction,
      GET_UPDATES: mockAction
    }
    store = new Vuex.Store({getters, actions})
  })

  it('should fetch an update on created', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router
    })
    expect(actions.fetchUpdate).toHaveBeenCalled()
  })
  it('should check if logged in user is the author of comment or reply', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router
    })
    let userId = 234
    let item = {
      created_by: {
        id: 234
      }
    }
    wrapper.vm.checkIfOwner(userId, item)
  })
  it('should clear comment text when cancelComment is called', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee'
      }
    })
    wrapper.vm.cancelCommenting()
    expect(wrapper.vm.commentBody).toBe(null)
  })
  it('should cancelediting a comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isCommentEditing: true
      }
    })
    wrapper.vm.cancelCommentEditing()
    expect(wrapper.vm.isCommentEditing).toBeFalsy()
  })
  it('should cancel editing a reply', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplyEditing: true
      }
    })
    wrapper.vm.cancelReplyEditing()
    expect(wrapper.vm.isReplyEditing).toBeFalsy()
  })
  it('should call action to edit a comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplyEditing: true
      }
    })
    wrapper.vm.commentBody = 'heehhee'
    wrapper.vm.makeCommentEdit()
    expect(actions.editUpdateComment).toBeCalled()
  })
  it('should call action to update a reply', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplying: true
      }
    })
    wrapper.vm.cancelReply()
    expect(wrapper.vm.isReplying).toBeFalsy()
  })

  it('should cancel replying to a comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplyEditing: true
      }
    })
  })

  it('should call action to post a reply update', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplyEditing: true
      }
    })
    wrapper.vm.makeReplyEdit()
    expect(wrapper.vm.isReplyEditing).toBeFalsy()
    expect(actions.editReplyToComment).toBeCalled()
  })
  it('should call action to post a reply', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplying: true
      }
    })
    wrapper.vm.replyBody = 'hello there'
    wrapper.vm.makeReply(23)
    expect(wrapper.vm.isReplying).toBeFalsy()
    expect(actions.replytoComment).toBeCalled()
    wrapper.vm.replyBody = ''
    wrapper.vm.makeReply(23)
    expect(wrapper.vm.replyError).toBeDefined()
  })

  it('should call action to post a comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplyEditing: true
      }
    })
    wrapper.vm.commentBody = 'heehhee'
    wrapper.vm.makeComment()
    expect(actions.commentOnUpdate).toBeCalled()
    wrapper.vm.commentBody = ''
    wrapper.vm.makeComment()
    expect(wrapper.vm.commentingError).toBeDefined()
  })
  it('should handle editing a reply', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplyEditing: false
      }
    })
    let reply = {
      id: 23,
      reply: 'wwew'
    }
    wrapper.vm.replyUpdateBody = 'wwew'
    wrapper.vm.selectedReply = 23
    wrapper.vm.handleEditingReply(reply)
    expect(wrapper.vm.isReplyEditing).toBeTruthy()
  })
  it('should handle replying to comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplying: false
      }
    })
    let reply = {
      id: 23,
      reply: 'wwew'
    }
    wrapper.vm.replyUpdateBody = 'wwew'
    wrapper.vm.selectedReply = 23
    wrapper.vm.handleReply(reply.id)
    expect(wrapper.vm.isReplying).toBeTruthy()
  })
  it('should handle deleting a comment reply', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplying: false
      }
    })
    let comment = {
      id: 23,
      reply: 'wwew'
    }
    let reply = {
      id: 23,
      reply: 'wwew'
    }
    wrapper.vm.replyUpdateBody = 'wwew'
    wrapper.vm.selectedReply = 23
    wrapper.vm.handleDeletingReply(comment, reply)
    expect(actions.deleteReply).toBeCalled()
  })
  it('should handle editing comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isCommentEditing: false,
        commentText: 'wwew'
      }
    })
    wrapper.vm.commentUpdateBody = () => 'wwew'
    wrapper.vm.selectedComment = 23
    wrapper.vm.handleEditingComment(23)
    expect(wrapper.vm.isCommentEditing).toBeTruthy()
  })
  it('should handle deleting a comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isCommentEditing: false,
        commentText: 'wwew'
      }
    })
    let comment = {
      id: 24
    }
    wrapper.vm.commentUpdateBody = () => 'wwew'
    wrapper.vm.selectedComment = 23
    wrapper.vm.handleDeletingComment(comment)
    expect(actions.deleteAComment).toBeTruthy()
  })
  it('should call action to like an update', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee',
        isReplyEditing: true
      }
    })
    wrapper.vm.userId = 21
    wrapper.vm.userUpdate = {
      user_likes: [21, 23, 45]
    }
    wrapper.vm.handleLikingUpdate()
  })
  it('should call action to like comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee'
      }
    })
    let comment = {
      id: 12,
      likes: 45
    }
    wrapper.vm.userId = 21
    wrapper.vm.userUpdate = {
      user_likes: [21, 23, 45]
    }
    wrapper.vm.inLikes = jest.fn()
    wrapper.vm.inLikes.includes = jest.fn()
    wrapper.vm.handleLikingComment(comment)
  })
  it('should call action to like a reply', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee'
      }
    })
    let reply = {
      id: 12,
      likes: 45
    }
    wrapper.vm.userId = 21
    wrapper.vm.userUpdate = {
      user_likes: [21, 23, 45]
    }
    wrapper.vm.inLikes = jest.fn()
    wrapper.vm.inLikes.includes = jest.fn()
    wrapper.vm.handleLikingReply(reply)
  })
  it('should call action to like comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee'
      }
    })
    let update = {
      id: 12,
      likes: 45
    }
    wrapper.vm.handleShowNext(update)
  })
  it('should call action to like comment', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router,
      data: {
        commentBody: 'heehhee'
      }
    })
    let update = {
      id: 23,
      likes: 45
    }
    wrapper.vm.updateList = [{id: 23}, {id: 24}, {id: 25}]
    wrapper.vm.findIndex = jest.fn()
    wrapper.vm.currentUpdatePosition = 0
    wrapper.vm.previousUpdatePosition = 1
    wrapper.vm.currentUpdate = 24
    wrapper.vm.handleShowPrevious(update)
  })
  it('watch for changes', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router
    })
    let update = {
      updated_by: {
        profile: {
          first_name: 'dsds',
          last_name: 'asasa'
        }
      },
      text: 'asasasass'
    }

    let comments = [{id: 23, comment: 'ewewe'}, {id: 24, comment: 'ewewe'}]
    wrapper.vm.$options.watch.getUpdate.call(wrapper.vm, update)
    expect(wrapper.vm.userUpdate).toBe(update)

    wrapper.vm.$options.watch.getComments.call(wrapper.vm, comments)
    expect(wrapper.vm.singleUpdateComments).toBe(comments)

    wrapper.vm.$options.watch.getUpdate.call(wrapper.vm, {updated_by: {
      id: 450
    }})

    expect(wrapper.vm.updateList).toEqual([{id: 23}, {id: 24}, {id: 25}])
    wrapper.vm.route = '$router.params.id'
    wrapper.vm.userUpdate.number_of_comments = 34
    let hasComments = wrapper.vm.hasComments
    expect(hasComments).toBeTruthy()
  })
  it('should compute update', () => {
    wrapper = shallowMount(SingleExpertUpdate, {
      store,
      localVue,
      router
    })
    let update = wrapper.vm.update
    expect(update).toBeDefined()
  })
})
