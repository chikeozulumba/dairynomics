import { createLocalVue, shallowMount } from '@vue/test-utils'
import SubscriptionCard from '@/pages/IndividualCurrentSubscription/SubscriptionCard.vue'

const propsData = {
  months: 45,
  farm_name: 'Joshua',
  subscriberPlan: {
    plan: 'free',
    createdAt: new Date(),
    dueDate: new Date(),
    expirationDays: 34
  }
}

const localVue = createLocalVue()

const wrapper = shallowMount(SubscriptionCard, { propsData, localVue })

describe('Subsciption Card Test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
