import { shallowMount, createLocalVue } from '@vue/test-utils'
import IndexPage from '@/pages/IndividualCurrentSubscription'
import Vuex from 'vuex'

const $router = {
  push: jest.fn()
}

const localVue = createLocalVue()
localVue.use(Vuex)

const actions = {
  getSubscriptionDetails: jest.fn()
}
const getters = {
  subscriptionInfo: () => ({
    isLoading: false,
    hasPlan: true,
    subscriptionDetails: {
      subscribed: {
        dueDate: new Date(),
        expirationDays: 45,
        plan: 'free',
        createdAt: new Date()
      }
    }
  })
}

const store = new Vuex.Store({
  getters,
  actions
})

const wrapper = shallowMount(IndexPage, {
  localVue,
  store,
  mocks: {
    $router
  }
})

describe('IndividualCurrentSubscription Page test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
