import messageModule from '@/store/modules/messageModule'
import PeerModel from '@/utils/peerModel/index'
import AxiosMockAdapter from 'axios-mock-adapter'
import axios from 'axios'

jest.mock('@/store')

const mock = new AxiosMockAdapter(axios)

const peerModel = new PeerModel()
peerModel.formatDate = jest.fn()
peerModel.closeConnection = jest.fn()
peerModel.connectPeer = jest.fn()
const commit = jest.fn()
const alertError = jest.fn()

const messages = [{
  id: 1094,
  sender_id: 3554,
  receiver_id: 3564,
  message: 'Something else',
  viewed: 0,
  created_at: '16:00PM June 14, 2019',
  updated_at: '08:48AM June 28, 2019',
  doReverse: false,
  status: true
}]

const mockUsers = [{
  'id': '3564',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'verified_account': 1,
  'unread_messages_count': 0,
  'is_seller': 0,
  'is_expert': 0
}, {
  'id': '3554',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'verified_account': 1,
  'unread_messages_count': 0,
  'is_seller': 0,
  'is_expert': 0
}]

const state = {
  strict: true,
  state: {
    isFetching: {
      page: false,
      messages: false
    },
    isTyping: false,
    chatMate: mockUsers[0], // active chat mate
    userPeerConnection: {},
    peerModel: peerModel,
    users: mockUsers,
    messagesMeta: {}
  }
}

describe('Message module', () => {
  afterEach(() => {
    mock.reset()
    messageModule.state.chatMate = {}
  })
  it('test store is properly setup', () => {
    expect(messageModule).toBeInstanceOf(Object)
    expect(messageModule.strict).toBeTruthy()
    expect(messageModule.state).toBeInstanceOf(Object)
    expect(messageModule.state.isFetching).toBeInstanceOf(Object)
    expect(messageModule.mutations).toBeInstanceOf(Object)
    expect(messageModule.actions).toBeInstanceOf(Object)
    expect(messageModule.getters).toBeInstanceOf(Object)
  })

  it('should mutate messages if messages was successfully saved', () => {
    messageModule.mutations.addMessageMutator(messageModule.state, messages[0])
    expect(messageModule.state.messages.length).toBeGreaterThan(0)
  })

  it('should mutate messages if messages was successfully saved after a resend', () => {
    messageModule.mutations.addMessageMutator(messageModule.state, {
      ...messages[0],
      isResend: true
    })
    expect(messageModule.state.messages.length).toBeGreaterThan(0)
  })

  it('should mutate messages if messages was successfully saved after a resend', () => {
    const stamp = new Date().getTime()
    messages[0].stamp = stamp
    messageModule.mutations.addMessageMutator(messageModule.state, {
      ...messages[0],
      isResend: true,
      stamp: stamp
    })
    expect(messageModule.state.messages.length).toBeGreaterThan(0)
  })

  it('text loadMessageMutator', () => {
    messageModule.mutations.loadMessagesMutator(messageModule.state, messages)
    expect(messageModule.state.messages.length).toBe(2)
  })

  it('should mutate signal and indicate if chat mate is typing', () => {
    messageModule.state.chatMate = mockUsers[0]
    messageModule.mutations.signalMutator(messageModule.state, {
      type: 'isTyping',
      signal: true,
      userId: mockUsers[0].id
    })
    expect(messageModule.state.isTyping).toBeTruthy()
  })

  it('should not mutate signal and not indicate if chat mate is typing', () => {
    messageModule.state.chatMate = mockUsers[1]
    messageModule.mutations.signalMutator(messageModule.state, {
      type: 'isTyping',
      signal: false,
      userId: mockUsers[1].id
    })
    expect(messageModule.state.isTyping).toBeFalsy()
  })

  it('should not mutate signal and not indicate if chat mate is typing', () => {
    messageModule.state.chatMate = {}
    messageModule.mutations.signalMutator(messageModule.state, {
      type: 'isTyping',
      signal: false,
      userId: mockUsers[1].id
    })
    expect(messageModule.state.isTyping).toBeFalsy()
  })

  it('should remove unread messages notifications with a valid sender id', () => {
    messageModule.state.users = mockUsers
    messageModule.mutations.unreadMessagesMutator(messageModule.state, {
      sender_id: mockUsers[0].id
    })
    expect(messageModule.getters.hasNewMessage(messageModule.state)).toBeFalsy()
  })

  it('should not remove unread messages notifications for inavlid sender id', () => {
    messageModule.state.users = mockUsers
    messageModule.mutations.unreadMessagesMutator(messageModule.state, {
      sender_id: 1000
    })
    expect(messageModule.getters.hasNewMessage(messageModule.state)).toBeFalsy()
  })

  it('should increment unread messages count', () => {
    state.users = [mockUsers[0]]
    state.chatMate = mockUsers[1]
    messageModule.mutations.unreadMessagesMutator(state, {
      sender_id: mockUsers[0].id,
      value: 1
    })
    expect(messageModule.getters.hasNewMessage(messageModule.state)).toBeTruthy()
  })

  it('should call chatMateMutator', () => {
    messageModule.mutations.chatMateMutator(messageModule.state, mockUsers[0])
    expect(messageModule.state.chatMate).toEqual(mockUsers[0])
  })

  it('should call updateUsersMutator', () => {
    messageModule.mutations.updateUsersMutator(messageModule.state, [{
      ...mockUsers[0],
      isActive: true
    }])
    expect(messageModule.state.users[0].isActive).toBeTruthy()
  })

  it('should call spinnerMutator without a value for status', () => {
    messageModule.mutations.spinnerMutator(messageModule.state, { type: 'page' })
    expect(messageModule.state.isFetching.page).toBeTruthy()
  })

  it('should call spinnerMutator with a value for status', () => {
    messageModule.mutations.spinnerMutator(messageModule.state, { type: 'page', status: true })
    expect(messageModule.state.isFetching.page).toBeTruthy()
  })

  it('should call updateUsersAction', () => {
    const commit = jest.fn()
    messageModule.actions.updateUsersAction({ commit }, [{
      ...mockUsers[0]
    }])
    expect(commit).toHaveBeenCalledTimes(1)
  })

  it('should call chatMateAction', () => {
    const commit = jest.fn()
    messageModule.actions.chatMateAction({ commit }, [{
      ...mockUsers[0]
    }])
    expect(commit).toHaveBeenCalledTimes(1)
  })

  it('should save message to database', async () => {
    const message = {
      message: 'Soemthing',
      senderId: mockUsers[1].id,
      reciverId: mockUsers[0].id,
      peerId: `${mockUsers[0].id}-${new Date().getTime()}`,
      stamp: new Date().getTime(),
      isResend: false
    }
    const response = {
      success: true,
      data: {},
      meta: {}
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/chat_messages').reply(201, response)
    await messageModule.actions.postMessageAction({ state: { peerModel }, commit }, message)
    expect(peerModel.formatDate).toBeCalled()
    expect(commit).toHaveBeenCalledTimes(1)
  })

  it('should return error and not save massage', async () => {
    const message = {
      message: 'Soemthing',
      senderId: mockUsers[1].id,
      reciverId: mockUsers[0].id,
      peerId: `${mockUsers[0].id}-${new Date().getTime()}`,
      stamp: new Date().getTime(),
      isResend: false
    }
    const response = {
      success: true,
      data: {},
      meta: {}
    }
    mock.onPost('https://beta.cowsoko.com/api/v/chat_messages').reply(500, response)
    await messageModule.actions.postMessageAction({ state: { peerModel, alertError }, commit }, message)
    const length = messageModule.state.messages.length
    expect(messageModule.state.messages[length > 0 ? length - 1 : 0]).toBeFalsy()
    expect(alertError).toHaveBeenCalled()
    expect(commit).toHaveBeenCalled()
  })

  it('should fetch users and also fetch peer ids', async () => {
    const form = new FormData()
    const getResponse = {
      success: true,
      data: mockUsers,
      meta: {}
    }
    const postResponse = {
      peer_ids: [
        {
          chat_id: '3564-1563216841177',
          user_id: '3564'
        }
      ]
    }
    mockUsers.forEach(({ id }, index) => {
      form.append(`peers[${index}]`, id)
    })
    mock.onGet('https://beta.cowsoko.com/api/v1/chat_peers').reply(200, getResponse)
    mock.onPost('https://beta.cowsoko.com/api/v1/chat_peers/chat_ids').reply(200, postResponse)
    await messageModule.actions.loadUsersAction({ state: { peerModel }, commit }, form)
    expect(peerModel.formatDate).toBeCalled()
    expect(commit).toHaveBeenCalled()
  })

  it('should fetch users and render users without having peer ids', async () => {
    const form = new FormData()
    const getResponse = {
      success: true,
      data: mockUsers,
      meta: {}
    }
    const postResponse = {
      peer_ids: []
    }
    form.append(`peers[0]`, 1000)
    mock.onGet('https://beta.cowsoko.com/api/v1/chat_peers').reply(200, getResponse)
    mock.onPost('https://beta.cowsoko.com/api/v1/chat_peers/chat_ids').reply(200, postResponse)
    await messageModule.actions.loadUsersAction({ state: { peerModel }, commit }, form)
    expect(messageModule.getters.allUsers(messageModule.state)[0].peerId).toBeUndefined()
    expect(commit).toHaveBeenCalled()
  })

  it('should fail to fetch users', async () => {
    const form = new FormData()
    const getResponse = {
      success: true,
      data: mockUsers,
      meta: {}
    }
    mock.onGet('https://beta.cowsoko.com/api/v/chat_peers').reply(200, getResponse)
    mock.onPost('https://beta.cowsoko.com/api/v1/chat_peers/chat_ids').reply(500)
    await messageModule.actions.loadUsersAction({ state: { peerModel, alertError }, commit }, form)
    expect(alertError).toBeCalled()
  })

  it('should call loadMessagesAction', async () => {
    const option = { showSpinner: true, page: 1 }
    const response = {
      success: true,
      data: messages,
      meta: {
        'current_page': 2,
        'from': null,
        'last_page': 1,
        'path': 'https://beta.cowsoko.com/api/v1/chat_messages',
        'per_page': 40,
        'to': null,
        'total': 9
      }
    }

    mock.onGet(`https://beta.cowsoko.com/api/v1/chat_messages?receiver_id=${mockUsers[0].id}/&page=1`).reply(200, response)
    state.alertError = alertError
    state.peerModel = peerModel
    state.peerModel.formatDate = jest.fn()
    state.chatMate = mockUsers[0]
    await messageModule.actions.loadMessagesAction({ state, commit }, option)
    expect(peerModel.formatDate).toBeCalled()
    expect(commit).toHaveBeenCalled()
  })

  it('should fail to fetch messages', async () => {
    const option = { showSpinner: false, page: 1 }
    state.alertError = alertError
    mock.onGet(`https://beta.cowsoko.com/api/v1/messages?receiver_id=${mockUsers[0].id}/&page=1`).reply(500)
    await messageModule.actions.loadMessagesAction({ state, commit }, option)
    expect(commit).toHaveBeenCalled()
  })

  it('should call mark messages as read if there are unread messages', async () => {
    mock.onPatch(`https://beta.cowsoko.com/api/v1/chat_messages/mark_read`).reply(200)
    state.users = [{...mockUsers[0], unread_messages_count: 3}]
    await messageModule.actions.markMessagesAsReadAction({ state, commit }, mockUsers[0].id)
    expect(commit).toHaveBeenCalled()
  })

  it('should call markMessageAsReadAction', async () => {
    mock.onPatch(`https://beta.cowsoko.com/api/v1/chat_messages/mark_read`).reply(200)
    state.users = [{...mockUsers[0], unread_messages_count: 0}]
    await messageModule.actions.markMessagesAsReadAction({ state, commit }, mockUsers[0].id)
    expect(commit).toHaveBeenCalled()
  })

  it('should not mark messages as read for a an unmatch user', async () => {
    mock.onPatch(`https://beta.cowsoko.com/api/v1/chat_messages/mark_read`).reply(200)
    state.users = [{...mockUsers[0], unread_messages_count: 2}]
    await messageModule.actions.markMessagesAsReadAction({ state, commit }, 1000)
    expect(messageModule.getters.allUsers(messageModule.state)[0]).toEqual({ ...mockUsers[0], isActive: true })
    expect(commit).toHaveBeenCalled()
  })

  it('should not mark messages as read', async () => {
    mock.onPatch(`https://beta.cowsoko.com/api/v1/chat_messages/mark_read`).reply(500)
    state.users = [{...mockUsers[1], unread_messages_count: 3}]
    await messageModule.actions.markMessagesAsReadAction({ state, commit }, mockUsers[1].id)
    expect(commit).toHaveBeenCalled()
  })

  it('should call openConnectionAction', () => {
    mock.onPatch(`https://beta.cowsoko.com/api/v1/chat_peers/chat_id`).reply(200)
    state.peerModel = peerModel
    const peerId = mockUsers[0].id
    peerModel.openConnection = jest.fn().mockImplementation((user, savePeerId) => savePeerId(peerId))
    messageModule.actions.openConnectionAction({ state, commit }, mockUsers[0])
    expect(peerModel.openConnection).toHaveBeenCalled()
  })

  it('should call connectPeer', () => {
    messageModule.actions.connectPeer({ state, commit }, mockUsers[0].peerId)
    expect(peerModel.connectPeer).toHaveBeenCalled()
  })

  it('should call closeConnection', () => {
    messageModule.actions.closeConnection({ state, commit }, mockUsers[0].peerId)
    expect(peerModel.closeConnection).toHaveBeenCalled()
  })

  it('should call allUsers', () => {
    expect(messageModule.getters.allUsers(state)).toEqual(state.users)
  })

  it('should call allMessages', () => {
    expect(messageModule.getters.allMessages(state)).toEqual(state.messages)
  })

  it('should call chatMate', () => {
    const state = { chatMate: mockUsers[0] }
    expect(messageModule.getters.chatMate(state)).toEqual(mockUsers[0])
  })

  it('should call isFetching', () => {
    expect(messageModule.getters.isFetching(state)).toBeFalsy()
  })

  it('should call isTyping', () => {
    expect(messageModule.getters.isTyping(state)).toBeFalsy()
  })

  it('should call hasNewMessage with unread messages count', () => {
    const state = { users: [{...mockUsers[0], unread_messages_count: 3}] }
    expect(messageModule.getters.hasNewMessage(state)).toBeTruthy()
  })

  it('should call peerModel', () => {
    const state = { peerModel }
    expect(messageModule.getters.peerModel(state)).toBeInstanceOf(PeerModel)
  })

  it('should call getMessagesMeta', () => {
    const state = { messagesMeta: {} }
    expect(messageModule.getters.getMessagesMeta(state)).toEqual({})
  })
})
