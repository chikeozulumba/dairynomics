import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import subscriptionModule from '@/store/modules/subscription'
import router from '@/router/router'

const URL = 'https://dairynomics.herokuapp.com/api'
router.push = jest.fn()

jest.mock('@/store')

const commit = jest.fn()
const mock = new AxiosMockAdapter(axios)
const state = {
  subscriptionDetails: {},
  isLoading: true
}

beforeEach(() => {
  mock.reset()
  commit.mockReset()
})
describe('subscription module test', () => {
  const { getters, actions, mutations } = subscriptionModule
  describe('Getters', () => {
    it('subscriptionInfo getters should return state', () => {
      const result = getters.subscriptionInfo(state)
      expect(result.isLoading).toBeTruthy()
    })
  })

  describe('mutations', () => {
    it('subscriptionDetails should set subscriptionDetails', () => {
      const result = mutations.subscriptionDetails(state, { name: 'joshua' })
      expect(result.name).toEqual('joshua')
    })

    it('loading should set isLoading to payload', () => {
      const result = mutations.loading(state, true)
      expect(result).toBeTruthy()
    })
  })

  describe('actions', () => {
    it('getSubscriptionDetails should call commit when status code is 200', () => {
      mock
        .onGet(`${URL}/subscriptions/user`)
        .reply(200, { success: true })
      actions.getSubscriptionDetails({ commit }, { router }).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('getSubscriptionDetails should call router.push when status code is not 200', () => {
      mock
        .onGet(`${URL}/subscriptions/user`)
        .reply(404, { success: false, status: 404 })
      actions
        .getSubscriptionDetails({ commit }, { router })
        .then({})
        .catch(() => {
          expect(router.push).toHaveBeenCalledWith('/')
        })
    })
  })
})
