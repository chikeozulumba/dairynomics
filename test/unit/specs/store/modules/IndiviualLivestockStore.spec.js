import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import axios from 'axios'
import AxiosCalls from '@/utils/api/AxiosCalls'
import * as helperFunctions from '@/utils/helperFunctions'
import MockAdapter from 'axios-mock-adapter'
import IndividualLivestockStore from '@/store/modules/individualLivestock.js'
import router from 'vue-router'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(router)
const mock = new MockAdapter(axios)

jest.mock('@/store')
jest.mock('@/utils/helperFunctions')

describe('IndividualLivestockStore', () => {
  it('should trigger the LIVE_STOCK_DETAILS method  component', () => {
    const state = { livestockDetials: {} }
    const payload = {
      age: '40',
      bodyCondition: 'Medium',
      breed: '',
      county: '',
      description: 'a healthy Cow',
      fieldTagNum: '',
      id: '',
      images:
        'https://media.mnn.com/assets/images/2017/01/cow-in-pasture.jpg.838x0_q80.jpg',
      postOn: '',
      price: '1234',
      sex: 'female',
      weigth: ''
    }
    const data = {
      weight: '',
      id: '',
      photo:
        'https://media.mnn.com/assets/images/2017/01/cow-in-pasture.jpg.838x0_q80.jpg',
      price: '1234',
      created_at: { date: '' },
      body_condition: 'Medium',
      description: 'a healthy Cow',
      sex: 'female',
      tag_num: '',
      age: '40',
      breed: '',
      county: ''
    }
    IndividualLivestockStore.mutations.LIVE_STOCK_DETAILS(state, data)
    expect(state.livestockDetials).toEqual(payload)
  })

  it('should trigger an error when token not provided', () => {
    const context = {
      commit: jest.fn()
    }
    IndividualLivestockStore.actions.getIndividualLivestock(context)
    mock.onGet('https://beta.cowsoko.com/api/v1/livestocks/3').reply(200, {
      weigth: '',
      id: '',
      market: '',
      price: '1234',
      postOn: '1st may',
      bodyCondition: 'Medium',
      description: 'a healthy Cow',
      sex: 'female',
      fieldTagNum: '',
      age: '40',
      breed: '',
      images: '',
      county: ''
    })

    IndividualLivestockStore.actions
      .getIndividualLivestock(context)
      .then(() => {
        expect(context.commit).toHaveBeenCalledTimes(1)
      })
  })

  it('should alert an error when api call returns an error message', async () => {
    const context = {
      commit: jest.fn()
    }
    const errorObj = { error: 404 }
    helperFunctions.alertError = jest.fn()

    AxiosCalls.awaitGet = jest.fn(() => errorObj)

    await IndividualLivestockStore.actions.getIndividualLivestock(context, '10000000')

    expect(helperFunctions.alertError).toHaveBeenCalledTimes(1)
  })

  it('should acommit data when API call returns valid value', async () => {
    const context = {
      commit: jest.fn()
    }
    const data = { id: 4 }
    AxiosCalls.awaitGet = jest.fn(() => data)

    await IndividualLivestockStore.actions.getIndividualLivestock(context, 1)
    expect(context.commit).toHaveBeenLastCalledWith('LIVE_STOCK_DETAILS', data)
  })

  it('should trigger avalaibleliveStock getter', () => {
    const state = {
      livestockDetials: {}
    }
    IndividualLivestockStore.getters.avalaibleliveStock(state)
  })
})
