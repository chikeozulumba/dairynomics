import UpdateProfileStore from '@/store/modules/UpdateProfileStore'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'

router.push = jest.fn()
let localStorageFn
jest.mock('@/store')
jest.mock('@/router/router')

const state = {
  experts: [],
  userUpdateStatus: false,
  updateError: null,
  updateSuccess: false,
  getUserDetails: {},
  successMessage: null
}
describe('mutation for updateprofilestore module', () => {
  beforeEach(() => {
    // localStorageFn.mockReset()
  })

  it('calls the setUserUpdateStatus mutation', () => {
    UpdateProfileStore.mutations.setUserUpdateStatus(state)
    expect(state.userUpdateStatus).toEqual(true)
  })

  it('calls the UpdateErrors mutation', () => {
    UpdateProfileStore.mutations.UpdateErrors(state, 'error')
    expect(state.updateError).toEqual('error')
  })

  it('calls the ProfileSuccess mutation', () => {
    UpdateProfileStore.mutations.ProfileSuccess(state, 'joshua')
    expect(state.updateSuccess).toEqual(true)
    expect(state.successMessage).toEqual('joshua')
  })

  it('calls the Details mutation', () => {
    UpdateProfileStore.mutations.Details(state, {id: 32})
    expect(state.getUserDetails).toEqual({id: 32})
  })
})
describe('Actions for UpdateProfileStore module', () => {
  afterEach(() => {
    mock.reset()
  })

  it('should successfully editUser profile', () => {
    const commit = jest.fn()
    const response = {
      success: true,
      data: [
        {
          first_name: 'Frank'
        }
      ]
    }

    mock.onPost('https://beta.cowsoko.com/api/v1/user/43').reply(200, response)
    localStorageFn = jest.fn(() => 43)
    Storage.prototype.getItem = localStorageFn
    UpdateProfileStore.actions.editUser({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
      expect(router.push).toHaveBeenCalledWith('/user-profile')
    })
  })

  it('should call commit updateErrors when success is false', () => {
    const commit = jest.fn()
    const response = {
      success: false
    }

    mock.onPost('https://beta.cowsoko.com/api/v1/user/43').reply(200, response)
    localStorageFn = jest.fn(() => 43)
    Storage.prototype.getItem = localStorageFn
    UpdateProfileStore.actions.editUser({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should call commit when error.response.data.message is Old Password Details missing', () => {
    const commit = jest.fn()
    const response = {
      message: 'Old Password Details missing'
    }

    mock.onPost('https://beta.cowsoko.com/api/v1/user/43').reply(404, response)
    localStorageFn = jest.fn(() => 43)
    Storage.prototype.getItem = localStorageFn
    UpdateProfileStore.actions.editUser({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should call commit when error.response.data.message is wrong old password details', () => {
    const commit = jest.fn()
    const response = {
      message: 'Wrong Old Password Details'
    }

    mock.onPost('https://beta.cowsoko.com/api/v1/user/43').reply(404, response)
    localStorageFn = jest.fn(() => 43)
    Storage.prototype.getItem = localStorageFn
    UpdateProfileStore.actions.editUser({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should call commit when error.response.data.message is otherwise', () => {
    const commit = jest.fn()
    const response = {
      message: 'other errors'
    }

    mock.onPost('https://beta.cowsoko.com/api/v1/user/43').reply(404, response)
    localStorageFn = jest.fn(() => 43)
    Storage.prototype.getItem = localStorageFn
    UpdateProfileStore.actions.editUser({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('userDetails action should call commit ', () => {
    const commit = jest.fn()
    const response = {
      success: true,
      data: [
        {
          first_name: 'Frank'
        }
      ]
    }

    mock.onGet('https://beta.cowsoko.com/api/v1/user/43').reply(200, response)
    localStorageFn = jest.fn(() => 43)
    Storage.prototype.getItem = localStorageFn
    UpdateProfileStore.actions.userDetails({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('userDetails action should call commit when the status code is above 300 ', () => {
    const commit = jest.fn()

    mock.onGet('https://beta.cowsoko.com/api/v1/user/43').reply(404)
    localStorageFn = jest.fn(() => 43)
    Storage.prototype.getItem = localStorageFn
    UpdateProfileStore.actions.userDetails({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  //   it('should return a 404 page when an error occurs', () => {
  //     const commit = jest.fn()
  //     const response = {
  //       success: false
  //     }
  //     mock.onGet('https://beta.cowsoko.com/api/v1/user_experts').reply(404, response)
  //     ExpertProfileModule.actions.GET_EXPERT_PROFILES({ commit }, { router }).then(() => {
  //       expect(router.push).toHaveBeenCalledWith('/404')
  //     })
  //   })

  //   it('should return a Home page when an an unknown error occurs', () => {
  //     const commit = jest.fn()
  //     const response = {
  //       success: false
  //     }
  //     mock.onGet('https://beta.cowsoko.com/api/v1/user_experts').reply(400, response)
  //     ExpertProfileModule.actions.GET_EXPERT_PROFILES({ commit }, { router }).then(() => {
  //       expect(router.push).toHaveBeenCalledWith('/')
  //     })
  //   })

//   it('should redirect to the home page for other errors from the server', () => {
//     const commit = jest.fn()
//     const response = {
//       success: false,
//       error: 'Some error'
//     }
//     mock.onGet('https://beta.cowsoko.com/api/v1/user_experts').reply(401, response)
//     ExpertProfileModule.actions.GET_EXPERT_PROFILES({ commit }, { router }).then(() => {
//       expect(router.push).toHaveBeenCalledWith('/')
//     })
//   })
})

describe('Getters for updateProfileStore Module', () => {
  it('gets user updateStatus', () => {
    const expectedState = UpdateProfileStore.getters.getUserUpdateStatus(state)
    expect(expectedState).toEqual(state)
  })
})
