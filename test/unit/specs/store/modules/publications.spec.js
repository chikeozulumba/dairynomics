import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import sinon from 'sinon'
import publicationsModule from '@/store/modules/publicationsModule'
import IndivualpublicationModule from '@/store/modules/IndivualPublicationModule'
import mockPublications from '@/store/modules/mocks/publications'
jest.mock('@/store')

describe('Publications module', () => {
  const state = {
    allPublications: {
      data: [],
      pagination: {
        currentPage: 1,
        lastPage: 5,
        total: 40
      },
      paginatedData: {}
    },
    featuredPublications: {
      data: [],
      pagination: {
        currentPage: 1,
        lastPage: 5,
        total: 40
      },
      paginatedData: {
        name: 'joshua'
      }
    },
    requestStatuses: {
      isDownloading: false,
      isFetching: false
    }
  }

  describe('Mutations on the publications module', () => {
    it('handles STORE_FETCHED_PUBLICATIONS mutation', () => {
      expect(state.allPublications.data).toEqual([])
      publicationsModule.mutations.STORE_FETCHED_PUBLICATIONS(
        state, {
          data: mockPublications
        }
      )
      expect(state.allPublications.data).toEqual(mockPublications)
      publicationsModule.mutations.STORE_FETCHED_PUBLICATIONS(
        state, {
          data: mockPublications,
          dataScope: 'featuredPublications'
        }
      )
      expect(state.allPublications.data).toEqual(mockPublications)
    })

    it('handles STORE_PAGINATED_PUBLICATIONS mutation', () => {
      const currentPage = 1
      const pageData = mockPublications
      publicationsModule.mutations.STORE_PAGINATED_PUBLICATIONS(
        state, {
          data: pageData,
          currentPage
        }
      )
      expect(state.allPublications.paginatedData[`page-${currentPage}`]).toEqual(mockPublications)

      publicationsModule.mutations.STORE_PAGINATED_PUBLICATIONS(
        state, {
          data: pageData,
          currentPage: currentPage,
          dataScope: 'featuredPublications'
        }
      )
      expect(state.featuredPublications.paginatedData[`page-${currentPage}`]).toEqual(mockPublications)
    })

    it('handles STORE_PAGINATION_DATA mutation', () => {
      let currentPage = state.allPublications.pagination.currentPage + 1
      publicationsModule.mutations.STORE_PAGINATION_DATA(
        state, {
          paginationData: {
            currentPage
          }
        }
      )
      expect(state.allPublications.pagination.currentPage).toEqual(currentPage)

      currentPage = state.featuredPublications.pagination.currentPage + 1
      publicationsModule.mutations.STORE_PAGINATION_DATA(
        state, {
          paginationData: {
            currentPage
          },
          dataScope: 'featuredPublications'
        }
      )
      expect(state.featuredPublications.pagination.currentPage).toEqual(currentPage)
    })

    it('handles CHANGE_REQUEST_STATUS mutation', () => {
      const isFetching = !state.requestStatuses.isFetching
      publicationsModule.mutations.CHANGE_REQUEST_STATUS(
        state, {
          isFetching
        }
      )
      expect(state.requestStatuses.isFetching).toEqual(isFetching)
    })
  })

  describe('Getters on the publications modules', () => {
    it('handles getPaginatedAllPublications() getter', () => {
      const {
        getters: {
          getPaginatedAllPublications
        }
      } = publicationsModule
      const publications = getPaginatedAllPublications(state)
      expect(publications).toBe(state.allPublications.paginatedData)
    })
    it('handles getAllPublications() getter', () => {
      const {
        getters: {
          getAllPublications
        }
      } = publicationsModule
      const publications = getAllPublications(state)
      expect(publications).toBe(state.allPublications.data)
    })
    it('handles getAllPublicationsPagination() getter', () => {
      const {
        getters: {
          getAllPublicationsPagination,
          getPaginatedFeaturedPublications,
          getFeaturedPublicationsPagination,
          getRequestStatuses
        }
      } = publicationsModule
      const pagination = getAllPublicationsPagination(state)
      const featuredPagination = getPaginatedFeaturedPublications(state)
      const featuredPublicationPagination = getFeaturedPublicationsPagination(state)
      const requestStatuses = getRequestStatuses(state)
      expect(pagination).toBe(state.allPublications.pagination)
      expect(featuredPagination.name).toBe('joshua')
      expect(featuredPublicationPagination).toBe(state.featuredPublications.pagination)
      expect(requestStatuses.isDownloading).toBe(false)
    })
  })
  describe('Actions on the publications module', () => {
    let mock = new AxiosMockAdapter(axios)
    const currentPage = 1
    const total = 72
    const lastPage = 8

    const mockedResponse = {
      data: {
        data: mockPublications,
        links: {
          first: 'https://beta.cowsoko.com/api/v1/publications?page=1',
          last: 'https://beta.cowsoko.com/api/v1/publications?page=8',
          prev: null,
          next: 'https://beta.cowsoko.com/api/v1/publications?page=2'
        },
        meta: {
          current_page: currentPage,
          from: 1,
          last_page: lastPage,
          path: 'https://beta.cowsoko.com/api/v1/publications',
          per_page: 10,
          to: 10,
          total
        }
      }
    }

    it('handles fetchPublications action', async () => {
      mock.onGet(`https://beta.cowsoko.com/api/v1/publications?page=${currentPage}`)
        .reply(200, mockedResponse.data)

      let commit = sinon.spy()
      const state = {
        allPublications: {
          pagination: {}
        }
      }

      const dataScope = 'allPublications'
      const {
        actions: {
          fetchPublications
        }
      } = publicationsModule
      await fetchPublications({
        commit,
        state
      }, {
        pageNumber: currentPage,
        dataScope
      })
      const paginationData = {
        lastPage,
        total,
        currentPage
      }

      expect(commit.args).toEqual([
        ['CHANGE_REQUEST_STATUS', {
          isFetching: true
        }],
        ['CHANGE_REQUEST_STATUS', {
          isFetching: false
        }],
        ['STORE_FETCHED_PUBLICATIONS', {
          data: mockedResponse.data.data,
          dataScope
        }],
        ['STORE_PAGINATED_PUBLICATIONS', {
          data: mockedResponse.data.data,
          currentPage,
          dataScope
        }],
        ['STORE_PAGINATION_DATA', {
          paginationData,
          dataScope
        }]
      ])

      // handle case where state.allPublications.pagination.currentPage is undefined
      state.allPublications.pagination.currentPage = 1
      commit = sinon.spy()
      await fetchPublications({
        commit,
        state
      }, {
        pageNumber: currentPage,
        dataScope
      })

      const newPaginationData = {
        lastPage,
        total
      }

      expect(commit.args).toEqual([
        ['CHANGE_REQUEST_STATUS', {
          isFetching: true
        }],
        ['CHANGE_REQUEST_STATUS', {
          isFetching: false
        }],
        ['STORE_FETCHED_PUBLICATIONS', {
          data: mockedResponse.data.data,
          dataScope
        }],
        ['STORE_PAGINATED_PUBLICATIONS', {
          data: mockedResponse.data.data,
          currentPage,
          dataScope
        }],
        ['STORE_PAGINATION_DATA', {
          paginationData: newPaginationData,
          dataScope
        }]
      ])

      // handle case where fetchPublication is called without currentPage argument
      commit = sinon.spy()
      await fetchPublications({
        commit,
        state
      }, {
        dataScope
      })
      expect(commit.args.length).toEqual(5)
    })

    it('should throw error when the response from server is 404', () => {
      mock.onGet(`https://beta.cowsoko.com/api/v1/publications?page=${currentPage}`)
        .reply(400)
      const commit = jest.fn()
      publicationsModule.actions.fetchPublications({commit, state
      }, mockPublications[0]).catch(error => {
        expect(error).toBeTruthy()
      })
    })

    it('handles successful downloadPublication action', async () => {
      const publication = mockPublications[0]
      mock.onGet(`https://beta.cowsoko.com/api/v1/publications/${publication.id}/download`)
        .replyOnce(200, { data: {message: 'success'} })

      const commit = sinon.spy()
      const state = {}
      window.URL.createObjectURL = jest.fn()
      const {
        actions: {
          downloadPublication
        }
      } = IndivualpublicationModule
      await downloadPublication({
        commit,
        state
      }, publication)

      const result = [
        ['LOADING_ICON', true],
        ['CHANGE_REQUEST_STATUS', {
          isDownloading: true
        }],
        ['PUBLICATION_ERROR', ''],
        ['CHANGE_REQUEST_STATUS', {
          isDownloading: false
        }],
        ['DOWNLOAD_PUBLICATION', { data: {message: 'success'} }],
        ['LOADING_ICON', false]
      ]

      expect(commit.args).toEqual(result)
    })

    it('handles failed downloadPublication action', async () => {
      const publication = mockPublications[0]
      const commit = sinon.spy()
      const state = {}

      // we specify wrong uri to trigger error
      mock.onGet(`https://beta.cowsoko.com/api/v1/publications/${publication.id}`)
        .replyOnce(200, {status: 400})
      jest.spyOn(window.URL, 'createObjectURL')
      window.URL.createObjectURL(jest.fn())
      const {
        actions: {
          downloadPublication
        }
      } = IndivualpublicationModule
      await downloadPublication({
        commit,
        state
      }, publication)

      expect(commit.args).toEqual([
        ['LOADING_ICON', true],
        ['CHANGE_REQUEST_STATUS', {
          isDownloading: true
        }],
        ['PUBLICATION_ERROR', ''],
        ['PUBLICATION_ERROR', undefined], ['LOADING_ICON', false]
      ])
    })

    it('handles incrementPage action', async () => {
      const {
        actions: {
          incrementPage
        }
      } = publicationsModule
      const commit = sinon.spy()
      const dataScope = 'allPublications'
      const currentPage = state[dataScope].pagination.currentPage + 1
      incrementPage({
        commit,
        state
      }, {
        dataScope
      })
      const paginationData = {
        currentPage
      }
      expect(commit.args).toEqual([
        ['STORE_PAGINATION_DATA', {
          paginationData,
          dataScope
        }]
      ])
    })

    it('handles decrementPage action', async () => {
      const {
        actions: {
          decrementPage
        }
      } = publicationsModule
      const commit = sinon.spy()
      const dataScope = 'allPublications'
      const currentPage = state[dataScope].pagination.currentPage - 1
      decrementPage({
        commit,
        state
      }, {
        dataScope
      })
      const paginationData = {
        currentPage
      }
      expect(commit.args).toEqual([
        ['STORE_PAGINATION_DATA', {
          paginationData,
          dataScope
        }]
      ])
    })
  })
})
