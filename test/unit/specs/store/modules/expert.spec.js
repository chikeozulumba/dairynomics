import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import expert from '@/store/modules/expert.js'

const localVue = createLocalVue()

localVue.use(Vuex)

const mock = new MockAdapter(axios)

jest.mock('@/store')

describe('expert module', () => {
  let state

  beforeEach(() => {
    state = {
      expertUpdates: []
    }
  })
  it('mutates SET_EXPERT_UPDATES when component is mounted', done => {
    const payload = {
      success: true,
      data: {
        updates: [
          {
            name: 'Test user',
            user_id: 210
          }
        ]
      }
    }
    expert.mutations.SET_EXPERT_UPDATES(state, payload)
    expect(state.expertUpdates).toEqual(payload.data.updates)
    done()
  })
  it('mutates CREATE_EXPERT_UPDATE when component is mounted', done => {
    const payload = {
      text: 'Test user',
      photo: [],
      user_id: 210
    }
    expert.mutations.CREATE_EXPERT_UPDATE(state, payload)
    expect(state.expertUpdates).toEqual([payload])
    done()
  })
  it('mutates SET_NUMBER_OF_EXPERT_UPDATES', done => {
    const payload = 20

    expert.mutations.SET_NUMBER_OF_EXPERT_UPDATES(state, payload)
    expect(state.numberOfExpertUpdates).toEqual(payload)
    done()
  })
  it('mutates TOGGLE_LOADING', done => {
    const payload = true

    expert.mutations.TOGGLE_LOADING(state, payload)
    expect(state.isLoading).toEqual(payload)
    done()
  })
  it('mutates SET_PAGE', done => {
    const payload = {
      currentPage: 1,
      LastPage: 1
    }

    expert.mutations.SET_PAGE(state, payload)
    expect(state.page).toEqual(payload)
    done()
  })
  it('mutates UPDATE_EXPERT_UPDATES', done => {
    const payload = {
      success: true,
      data: {
        updates: [
          {
            name: 'Test user',
            user_id: 210
          }
        ]
      },
      meta: {
        currentPage: 1,
        LastPage: 2
      }
    }

    expert.mutations.UPDATE_EXPERT_UPDATES(state, payload)
    expect(state.expertUpdates).toEqual(payload.data.updates)
    done()
  })
  it('mutates UPDATE_EXPERT_UPDATES but does not update expertUpdates', done => {
    const payload = {
      success: false,
      data: {
        updates: [
          {
            name: 'Test user',
            user_id: 210
          }
        ]
      },
      meta: {
        currentPage: 2,
        LastPage: 3
      }
    }

    expert.mutations.UPDATE_EXPERT_UPDATES(state, payload)
    expect(state.expertUpdates).toEqual(payload.data.updates)
    done()
  })
  it('mutates UPDATE_REQUEST_STATUS', done => {
    const payload = true

    expert.mutations.UPDATE_REQUEST_STATUS(state, payload)
    expect(state.isFetching).toEqual(payload)
    done()
  })
})
describe('Actions for expert module', () => {
  afterEach(() => {
    mock.reset()
  })
  it('successfully fetches expert updates from the API', () => {
    const commit = jest.fn()
    const mock = new MockAdapter(axios)
    const response = {
      success: false,
      data: {
        updates: [
          {
            name: 'Test user',
            user_id: 1878
          }
        ]
      },
      meta: {
        currentPage: 1,
        LastPage: 1
      }
    }
    mock
      .onGet('https://beta.cowsoko.com/api/v1/experts/1878/updates')
      .replyOnce(200, response)
    expert.actions.GET_EXPERT_UPDATES({ commit }).then(() => {
      expect(commit).toHaveBeenCalled(1)
    })
  })
  it('successfully fetches more expert updates from the API', () => {
    const commit = jest.fn()
    const mock = new MockAdapter(axios)
    const response = {
      success: false,
      data: {
        updates: [
          {
            name: 'Test user',
            user_id: 1878
          }
        ]
      },
      meta: {
        currentPage: 2,
        LastPage: 2
      }
    }
    mock
      .onGet('https://beta.cowsoko.com/api/v1/experts/1878/updates?page=2')
      .replyOnce(200, response)
    expert.actions.UPDATE_EXPERT_UPDATES({ commit }).then(() => {
      expect(commit).toHaveBeenCalled(1)
    })
  })
})
describe('Expert updates getter', () => {
  let state
  beforeEach(() => {
    state = {
      expertUpdates: [],
      numberOfExpertUpdates: 11,
      isLoading: false,
      page: {
        currentPage: 2,
        LastPage: 2
      },
      isFetching: false
    }
  })
  it('should return the expertUpdates; []', () => {
    const result = expert.getters.getExpertUpdates(state)
    expect(result).toEqual([])
  })
  it('should return the numberOfExpertUpdates; "11"', () => {
    const result = expert.getters.getNumberOfExpertUpdates(state)
    expect(result).toEqual(11)
  })
  it('should return the getIsFetching; "false"', () => {
    const result = expert.getters.getIsFetching(state)
    expect(result).toEqual(false)
  })
  it('should return the getExpertUpdatesPage; "{page...}"', () => {
    const result = expert.getters.getExpertUpdatesPage(state)
    expect(result).toEqual(state.page)
  })
})
