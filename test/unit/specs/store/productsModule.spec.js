import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import productsModule from '@/store/modules/productsModule'
jest.mock('@/store')

const commit = jest.fn()
const mock = new AxiosMockAdapter(axios)

const pageMeta = {
  current_page: 2,
  last_page: 43
}

const mockState = {
  products: [],
  counties: [],
  paginationInfo: {
    currentPage: 4,
    lastPage: 45
  },
  isLoading: false
}

const payload = {
  pageNumber: 4,
  county: 'joshua',
  selectedType: 'jos'
}

describe('Products Module Test', () => {
  beforeEach(() => {
    mock.reset()
  })
  describe('Mutation test', () => {
    it('state should be equal to payload', () => {
      let payload = [ {id: '122', 'title': 'ddsdsd', viewed: '0'} ]
      productsModule.mutations.STORE_COUNTIES(productsModule.state, payload)
      expect(productsModule.state.counties).toBe(payload)
    })

    it('state.isLoading should be false', () => {
      productsModule.mutations.STORE_PRODUCTS(productsModule.state, [])
      expect(productsModule.state.isLoading).toBe(false)
      expect(productsModule.state.products).toEqual([])
    })

    it('STORE_PAGINIATION_INFO should set currentPage', () => {
      productsModule.mutations.STORE_PAGINATION_INFO(productsModule.state, pageMeta)
      expect(productsModule.state.paginationInfo.currentPage).toBe(2)
      expect(productsModule.state.paginationInfo.lastPage).toEqual(43)
    })

    it('LOADING_PRODUCTS should set isLoading to payload', () => {
      productsModule.mutations.LOADING_PRODUCTS(productsModule.state, true)
      expect(productsModule.state.isLoading).toBeTruthy()
    })
  })

  describe('Getters Test', () => {
    it('should return showPrevious state', () => {
      const expectedResult = productsModule.getters.showPrevious(mockState)
      expect(expectedResult).toBeTruthy()
    })

    it('should return showNext state', () => {
      const expectedResult = productsModule.getters.showNext(mockState)
      expect(expectedResult).toBeTruthy()
    })
  })

  describe('Actions Test', () => {
    it('fetchCounties should commit store_counties', () => {
      const response = {
        request: {
          counties: []
        }
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/counties').reply(200, response)

      productsModule.actions.fetchCounties({commit})
      expect(commit).toHaveBeenCalled()
    })

    it('should throw error when api call is not successful', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/counties').reply(500)

      productsModule.actions.fetchCounties({commit}).then({}).catch(error => {
        expect(error).toBeDefined()
      })
    })

    it('fetchProducts should commit store_products', () => {
      const response = {
        data: {
          products: [],
          meta: 'joshua'
        }
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/products?page=4&county=joshua&type=jos').reply(200, response)

      productsModule.actions.fetchProducts({commit}, payload)
      expect(commit).toHaveBeenCalled()
    })

    it('fetchProducts should commit store_products when selectedType is null', () => {
      const response = {
        data: {
          products: [],
          meta: 'joshua'
        }
      }
      const alteredPayload = {...payload, selectedType: null}
      mock.onGet('https://beta.cowsoko.com/api/v1/products?page=4&county=joshua&type=all').reply(200, response)

      productsModule.actions.fetchProducts({commit}, alteredPayload)
      expect(commit).toHaveBeenCalled()
    })

    it('should throw error when api call is not successful', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/products?page=4&county=joshua&type=jos').reply(500)

      productsModule.actions.fetchProducts({commit}, payload).then({}).catch(error => {
        expect(error).toBeDefined()
      })
    })
  })
})
