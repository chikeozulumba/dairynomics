import {shallowMount, createLocalVue} from '@vue/test-utils'
import SingleInvoice from '../../../../src/components/ui/SingleInvoice'
import ModalBox from '../../../../src/components/ui/ModalBox'
import Router from 'vue-router'
const localVue = createLocalVue()
localVue.use(Router)
const router = new Router()

describe('modal box component', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(ModalBox, {router})
  })
  it('modal box should render without fail', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
describe('modal box component', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(SingleInvoice, {router})
  })
  it('single invoice should render without fail', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
