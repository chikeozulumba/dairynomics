import { shallowMount, createLocalVue } from '@vue/test-utils'
import Payment from '@/pages/Subscriptionoption/PaymentProcess'
import Vuex from 'vuex'
import router from '@/router/router'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(router)

const getters = {
  getSubscriptionInfo: () => ({
    subscription: ['bronze'],
    isLoading: false
  }),
  USER_PROFILE: () => ({
    first_name: 'bagenda',
    last_name: 'deo'
  })
}

const actions = {
  makeSubscription: jest.fn()
}

const store = new Vuex.Store({
  actions,
  getters
})

const wrapper = shallowMount(Payment, { localVue, store, router })
wrapper.setData({ contact: '0700555888', countrycode: '+256' })

wrapper.vm.$router.push = jest.fn()

describe('PaymentProcess Page tests', () => {
  beforeAll(() => {
    window.sessionStorage.setItem('month', 12)
    window.sessionStorage.setItem('value', 1000)
  })

  it('should render when all the data values are initialized', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })

  it('test if this.makeSubscription was called', () => {
    const makePaymentSpy = jest.spyOn(wrapper.vm, 'makePayment')
    makePaymentSpy()
    expect(actions.makeSubscription).toBeTruthy()
  })

  it('test if active number is set', () => {
    const makePaymentSpy = jest.spyOn(wrapper.vm, 'makePayment')
    makePaymentSpy()
    const activateTabSpy = jest.spyOn(wrapper.vm, 'activateTab')
    activateTabSpy(2)
    expect(wrapper.vm.active).toEqual(2)
  })

  it('test if tryagain button was called', () => {
    const tryAgainButtonSpy = jest.spyOn(wrapper.vm, 'tryAgainButton')
    tryAgainButtonSpy()
    expect(wrapper.vm.active).toEqual(1)
  })

  it('test if continue button was called', () => {
    const continueButtonSpy = jest.spyOn(wrapper.vm, 'continueButton')
    continueButtonSpy()
    expect(wrapper.vm.message).toEqual('')
  })
})
