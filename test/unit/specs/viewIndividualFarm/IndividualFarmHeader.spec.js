import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import IndividualFarmHeader from '@/pages/ViewIndividualFarm/IndividualFarmHeader'
import mockStore from '@/store/modules/mocks/IndividualFarmStore'

const localVue = createLocalVue()

localVue.use(Vuex)

jest.mock('@/store')

const wrapper = shallowMount(IndividualFarmHeader, {store: mockStore, localVue})

describe('Individual Farm Header.vue', () => {
  it('should render without errors', () => {
    expect(wrapper.vm.$el).toMatchSnapshot()
  })
})
