import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import ProfileTests from '@/pages/ProfileTests'
import mockUser from '../publications/__mock__/user'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Router)
const router = new Router()
const state = {
  user: mockUser,
  userTests: [
    {
      title: 'Herds Composition',
      testComponent: 'HerdsComposition'
    },
    {
      title: 'Learn how much money you can make from farming',
      testComponent: 'FarmStats'
    }
  ],
  sortedResults: {},
  userTestResults: []
}
const getters = {
  USER_PROFILE: () => ({
    id: 45
  }),
  ALL_USER_TESTS: () => ['joshua', 'fredrick'],
  SORTED_TEST_RESULTS: () => ({name: 'joshua'}),
  ALL_USER_RESULTS: () => ['joshua'],
  PROFILE_PIC: () => 'https://joshua.jpg'
}
const actions = {
  GET_USER_PROFILE: jest.fn(),
  GET_USER_TEST_RESULTS: jest.fn()
}

describe('ProfileTests Component when user has no test results', () => {
  const store = new Vuex.Store({
    state,
    getters,
    actions
  })
  const profileTestsPage = mount(ProfileTests, { store, localVue, router })

  it('renders all tests present in the store', () => {
    expect(profileTestsPage.findAll('.test-item').wrappers.length).toEqual(2)
  })
  it('renders the right image when test has not yet been taken', () => {
    expect(profileTestsPage.findAll('.test-item-img').wrappers.length).toEqual(2)
  })
  it('attempts to get user\'s test results from the backend', () => {
    expect(actions['GET_USER_TEST_RESULTS'].mock.calls.length).toBeGreaterThan(0)
  })
})

describe('ProfileTests Component when user has test result', () => {
  const sortedResults = { 'Herds Composition': { results: '30' } }
  const store = new Vuex.Store({
    state: { ...state, sortedResults },
    getters,
    actions
  })
  const profileTestsPage = mount(ProfileTests, { store, localVue, router })

  it('renders the right image when test has been taken', () => {
    expect(profileTestsPage.find('.meter-container > img').exists()).toBe(false)
  })
  it('still attempts to get user\'s latest test results from the backend', () => {
    expect(actions['GET_USER_TEST_RESULTS'].mock.calls.length).toBeGreaterThan(0)
  })
})
