import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'
import AverageMilkProduction from '@/pages/AverageMilkProductionPage/index.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(VueRouter)
localVue.use(Vuelidate)

describe('AverageMilkProductionPage', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(AverageMilkProduction, {
      stubs: ['router-link', 'router-view'],
      data: {
        title: 'Milk Production',
        numOfSoldMilk: 5,
        numOfProdMilk: 6,
        section: 'prod-milk',
        performance: '',
        errorMessage: '',
        previousSectionLink: false
      }
    })
  })
  it('should render the component AverageMilkPerformance', () => {
    wrapper.setProps({ performance: 60 })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should successfully switch between sections and call the watch methods', () => {
    wrapper = shallowMount(AverageMilkProduction)
    wrapper.setData({ section: 'prod-milk' })
    expect(wrapper.vm.section).toBe('prod-milk')
    wrapper.setData({ section: 'sold-milk' })
    expect(wrapper.vm.section).toBe('sold-milk')
    wrapper.setData({ section: 'prod-milk' })
    expect(wrapper.vm.section).toBe('prod-milk')
  })

  it('should trigger the submitForm method', () => {
    wrapper.setData({ section: 'sold-milk', numOfProdMilk: 6, numOfSoldMilk: 5 })
    wrapper.vm.submitForm(10)
    expect(wrapper.vm.numOfSoldMilk).toBe(10)

    wrapper.setData({ section: 'prod-milk', numOfProdMilk: 6, numOfSoldMilk: 5 })
    wrapper.vm.submitForm(6)
    expect(wrapper.vm.numOfProdMilk).toBe(6)

    wrapper.setData({ section: 'sold-milk', numOfProdMilk: 6, numOfSoldMilk: 7 })
    wrapper.vm.submitForm(5)
  })
  it('should trigger the previousSection method', () => {
    wrapper.setData({ section: 'sold-milk' })
    wrapper.vm.previousSection()
  })
})
