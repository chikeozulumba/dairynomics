import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Vuelidate from 'vuelidate'
import AverageMilkProduction from '@/components/ui/AverageMilkProduction.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Vuelidate)

describe('AverageMilkProduction', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(AverageMilkProduction, {
      data: () => {
        return {
          milkCount: '',
          validator: {
            isError: false,
            message: ''
          }
        }
      },
      event: {
        $emit: jest.fn()
      }
    })
  })
  it('should render the component AverageMilkProduction', () => {
    wrapper.setData({ milkCount: 60 })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should call handleSubmit method', (done) => {
    // const spy = jest.spyOn(wrapper.vm, 'updateErrorMessage')
    wrapper.setData({ milkCount: 60 })
    wrapper.vm.handleSubmit()
    expect(wrapper.vm.validator.isError).toBe(false)

    wrapper.setData({ milkCount: 0 })
    wrapper.vm.handleSubmit()
    // expect(spy).toHaveBeenCalled()
    wrapper.vm.$nextTick(() => {
      expect(wrapper.vm.validator.isError).toBe(true)
      done()
    })
  })

  it('should find the back button and call the previousSection method', () => {
    const spy = jest.spyOn(wrapper.vm, 'previousSection')
    wrapper.setProps({ previousSectionLink: true })
    wrapper.setData({
      milkCount: '',
      validator: {
        isError: false,
        message: ''
      }
    })
    wrapper.find('#previousSectionLink').trigger('click')
    wrapper.vm.previousSection()
    expect(spy).toHaveBeenCalled()
  })
  it('should ', () => {
    wrapper.setData({
      isDisabled: false,
      milkCount: '50',
      validator: {
        isError: false,
        message: ''
      }
    })
    wrapper.setProps({ numOfProd: 30 })
    wrapper.setData({ milkCount: 80 })
    expect(wrapper.vm.validator.isError).toBe(true)
  })
})
