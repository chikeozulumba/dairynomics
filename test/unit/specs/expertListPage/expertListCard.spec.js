import { shallowMount } from '@vue/test-utils'
import ExpertListCard from '@/pages/ExpertListPage/ExpertListCard'

describe('ExpertListCard', () => {
  const mockedRouter = { push: jest.fn() }
  const wrapper = shallowMount(ExpertListCard, {
    beforeCreate () {
      this.$router = mockedRouter
    },
    propsData: { id: 5 }
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render the required html tags', () => {
    expect(wrapper.contains('button')).toBe(true)
    expect(wrapper.contains('h5')).toBe(true)
    expect(wrapper.contains('button')).toBe(true)
    expect(wrapper.contains('img')).toBe(true)
    expect(wrapper.findAll('p')).toHaveLength(3)
  })

  it('link to the expert page when button is clicked', () => {
    wrapper.find('button').trigger('click')
    expect(mockedRouter.push).toHaveBeenCalledWith('/expert-profile/5')
  })
})
